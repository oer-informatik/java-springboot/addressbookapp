# Persistenzschicht und Model

[Dieser Text ist Bestandteil eines Tutorials zur Erstellung einer einfachen AdressbuchApp mit SpringBoot/Java](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

## Konfiguration und Abhängigkeiten

Der Ablauf beim Einfügen einer neuen Komponente ist bei Spring(Boot) weitgehend identisch:

- Abhängigkeiten in der `pom.xml` eintragen

- neue Klasse erzeugen, die mit einer `@Component`-Annotation annotiert wird

- Konfiguration anpassen - z.B. über `application.properties` im Ordner `src/main/ressources`

Um ein Model mit zugehörigem Datenspeicher (Persistenzschicht) zu erstellen müssen zwei Abhängigkeiten ergänzt werden:

- Spring Data JPA

- Eine Persistence Unit - hier der Einfachheit halber zunächst die in-memory-Datenbank H2

Folgende Abschnitte müssen  in der `pom.xml` vorhanden sein:

```xml
<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
      <groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
      <scope>runtime</scope>
</dependency>
```
(NB: händisch reinkopieren, IJ: kopieren oder über Code/Generate => Dependency nach "spring-boot-starter-data-jpa" und "com.h2database" suchen)

## Gliederung des Projekts in Packages

Es bestehen für ein Projekt zwei grundsätzliche Ansätze, wie die einzelnen Klassen in Packages strukturiert werden:

- Gliederung nach Begriffen der Problem- oder Fachdomäne: alle Klassen, die zu einem bestimmten Teil der Fachdomäne gehören werden in einem Package zusammengefügt. Also etwa alle Dateien, die eine Adresse verarbeiten.

- Gliederung nach Komponenteneingenschaften: Klassen, die einen gemeinsamen _Concern_ behandeln werden in Packages zusammengefasst: also etwa alle Controller, alle Modell und alle Views.

Für beide Varianten gibt es Vor- und Nachteile. Wir erstellen hier eine Struktur basiernd auf der Fachdomäne: alle Adress-Klassen werden in einem Package zusammengefasst.

Hierzu muss zunächst ein neues Package erstellt werden:

![](images/06_NewPackage.png)

...und einen Namen erhalten:

![](images/07_PackageName.png)

## Die Model-Klasse
In dem neuen Package erzeugen wir eine Java-Klasse, die unser Model repräsentiert. Das Model wird als _POJO_ umgesetzt, das zusätzlich mit Annotationen versehen wird. Als _POJO_ (_Plain Old Java Object_) im engeren Sinne werden einfache Java-Klassen verstanden, die nur von `Object` erben, keine Interfaces implementieren und nicht annotiert sind. Häufig verfügen _POJOs_ nur über Attribute, Getter und Setter.

Es wird im neuen Package eine neue Klasse erstellt:

![](images/06_NewJavaClass.png)

...die auf den Namen Address hört:

![](images/M00_NewAddressModel.png)

Ein Model wird mit der Annotaion `@Entity` versehen - und damit hat JPA im Prinzip schon alle Information, die es benötigt.

```java
@Entity
public class Address { /* */ }
```

Damit die Annotation `@Entity` bekannt ist muss nur noch die Persistenzumgebung importiert werden. Wer Maven/Netbeans danach suchen lassen will, der wird im Repository "jakarta.persistence" fündig:

![](images/07_ImportPersistenceJakarta.png)

Schließlich liegt die Annotation im Paket javax.persistence:

![](images/07_ImportPersistenceJavaX.png)


```java
import javax.persistence.*;
```


Trägt die Tabelle in der Datenbank einen anderen Namen als die Klasse (z.B. das deutsche Adressen statt englisch _address_), so kann dieser mit der Annotation `@Table(name = "Adressen")` festgelegt werden.

Es folgt die Deklaration der Attribute:
```java
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;

    private String lastName;
```

Auch die Felder können annotiert werden, i.d.R. ist dies aber nur beim Primärschlüssel (`id`) erforderlich.
Die Annotation `@Column` bietet einige Parameter, die interessant sein könnten:

- `String name`: Der Name des Attributs in der Tabelle (Spaltenname) weicht vom Klassenattributnamen ab

- `boolean unique`: bei `unique=true` muss der Wert einzigartig sein

- `boolean nullable`: bei `nullable=false` darf der Attributwert nicht `NULL` sein

- `boolean updatable / insertable`: Attribut wird in UPDATE / INSERT-Befehlen ignoriert, wenn diese _flag_ expizit auf `false` gestellt wird (`updatable=false`)

- `String table`: Wenn dieses Attribut in einer anderen Tabelle als die Primäre Tabelle gespeichert wird, kann der Name hier angegeben werden.

- `int length`: Wenn die Zeichenlänge eines Strings von max. 255 abweicht, dann dies hiermit angegeben werden.

- `int precision / scale`: Gibt die Anzahl der Nachkommastellen an, falls der Datentyp `Decimal` genutzt wird (muss analog zur Angabe per DDL gesetzt werden)

Eine sinnvolle Annotation eines Attributs könnte also sein:

```java
    @Column(nullable = false, unique = false, name="Nachname")
    private String lastName;
  ```

  Darüber hinaus müssen Konstruktoren eingefügt werden: ein Default-Konstruktor (weil die JPA es verlangt, dieser wird jedoch nicht genutzt) und ein Konstruktor, der alle Attribute setzt, die nicht mit `@GeneratedValue` annotiert sind:

 Netbeans bietet uns im Kontextmenü (rechte Maustaste im Editorfenster) eine Reihe von automatischer Code-Generierung an, die wir hierzu nutzen können:

 ![](images/07_InsertCode.png)

 ![](images/07_InsertCodeMenu.png)

 Im Fall des Konstruktors sollten alle Attribute bis auf id ausgewählt werden.

 ![](images/07_InsertCodeConstructor.png)

Den Default-Konstruktor ohne Parameter müssen wir manuell eingeben:

```java
    private Address() {
    }

    public Address(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
```

Es gehört zum guten Stil, dass man die `toString()`-Methode ebenso überschreibt und alle relevanten Attribute ausgibt:

```java
    @Override
    public String toString() {
        return String.format(
                "Address[id=%d, firstName='%s', lastName='%s']", id, firstName, lastName);
    }
```

Weiterhin müssen noch Getter für alle Attibute ergänzt werden:

```java
    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
```

Ggf. sind auch die einzelnen Setter-Methoden wichtig. Für die Aktualisierung per PUT auf jeden Fall der Setter für die `id`:
```java
void setId(Long id) {
   this.id = id;
}
```

## Erstellen eines neuen Repositories

Repositories sind Komponenten, die bestimmte fachliche Klassen verwalten. Oft erweitern sie das `CrudRepoistory`, dass allerlei Methoden zum Verabeiten der Objekte dieser Klassen anbietet. Hierzu werden die generische Typparameter von `CrudRepoistory` mit dem Model und dem id-Typen typisiert.

In Netbeans kann unter `New`->`Others` ein Repository Interface gewählt werden, in IntelliJ muss das händisch eingegeben werden.

![](images/M01_NewRepositoryInterface.png)

... der Name des Repositories eingegeben sowie Model-Name und Datentyp des Keys (hier: `Long`):

![](images/M02_NewRepositoryInterface.png)

Das erzeugte Interface sollte etwa so aussehen:

```java
package de.csbme.IFAXX.myAddressBookApp.address;

import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {}  
```
Für den Anfang müssen wir hier nichts mehr anpassen. Das Interface selbst wird auch nie direkt implementiert. Wir können uns zunächst vorstellen, dass das Spring-Framework eine eigene Implementierung generiert, die unsere Address-Klasse nutzt:

![](plantuml/AddressRepository.png)


Es mag erstaunen, dass diese Repository-Interfaces nie direkt implementiert werden müssen: Spring sucht zunächst Implementierungen, versucht dann über _Spring Data_ Abfragen automatisch zu generieren oder zuletzt die Methoden der Refenzimplementierung `SimpleJpaRepository` zu nutzen.

![](plantuml/RepositoryInterface.png)

## Vertiefung der Annotationen für das Modell
### Erweiterung der Annotationen für Attribute
Es gibt eine ganze Reihe weiterer Annotationen, die verwendet werden können. Zum Teil sind diese an eine spezielle Implementierung der JPA (i.d.R. Hibernate) gebunden und nicht generell gültig.

- `@GeneratedValue(strategy = GenerationType.AUTO)`: hier können auch noch die Strategien `IDENTITY`, `SEQUENCE`, und `TABLE` ausgewählt werden.

- `@NotNull`: kann auf Methoden, Attribute oder Parameter angewendet werden (bei Attributen kann auch `@Column(nullable=false)` gesetztw werden)

- `@NaturalId`: Sofern das Domänenmodell über einen natürlichen Schlüssel verfügt kann dieser hiermit annotiert werden.

- `@NotEmpty`: für Zeichenketten - verhindert Leerstrings.

- `@Size(min = 10, max = 32)`: gibt an, wie lange eine Zeichenkette minimal / maximal sein darf

## Nächste Schritte

Als nächstes sollte das Modell und das Repository einmal benutzt werden, um die Funktionalität zu verstehen und zu prüfen, ob alles so klappt, wie es soll. Dazu muss

- Eine SpringBoot-Testumgebung geschaffen

- für die wesentlichen Methoden (CRUD) jeweils ein Test geschrieben werden

[Die weiteren Schritte finden sich in diesem Tutorial (link)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/01a_AutomatischesTestenDesRepositories.html)

## Links und weitere Informationen

## _Quellen und offene Ressourcen (OER)_
Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/java-springboot/addressbookapp]() und sind zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
