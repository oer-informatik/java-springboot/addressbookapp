# Die Template-Engine Thymeleaf

[Dieser Text ist Bestandteil eines Tutorials zur Erstellung einer einfachen AdressbuchApp mit SpringBoot/Java](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

## Ziel dieses Abschnitts:

Um die Grundstrukturen der Thymeleaf-Einbindung kennen zu lernen soll:

- eine HTML-Vorlage (Template) mit Platzhaltern erstellt werden

- die Platzhalter mit Werten über die URL, Request-Parameter und `application.properties` ersetzt werden

- statische Inhalte (Bilder, CSS-Definitionen) eingebunden werden.


## Einbinden der Abhängigkeiten und Konfigurationen:

Um die HTML-Template-Engine Thymeleaf nutzen zu können müssen zunächst die Anhängigkeiten ergänzt werden.
Hierzu muss - analog zu den bisherigen Abhängigkeiten wie _H2_ und _jUnit_ in der Datei `pom.xml` (im Ausgangsverzeichnis der App) zwischen den Tags `<dependencies>` und  `</dependencies>` zwei neue XML-`<dependency>`-Elemente eingefügt werden:

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <optional>true</optional>
</dependency>
```

Da wir beim Entwickeln häufige Änderungen direkt sehen wollen, bietet es sich an, mit der zweiten Abhängigkeit (das `devtool`-Paket) den internen Cache zu deaktivieren ([nach Auskunft von mykong klappt dies bei IntelliJ nicht:](https://mkyong.com/spring-boot/intellij-idea-spring-boot-template-reload-is-not-working/)).

Darüber hinaus sollten eine Reihe von Konfigurationen gesetzt werden. Am einfachsten geht dies, in dem die Datei `application.properties` (sie liegt in `\src\main\resources`) ergänzt wird:
```ini
spring.thymeleaf.cache=false
spring.thymeleaf.enabled=true
spring.thymeleaf.prefix=classpath:/templates/
spring.thymeleaf.suffix=.html

spring.application.name=Mein Adressbuch
```

## HTML-Templates anlegen

Thymeleaf ergänzt ein HTML-Gerüst um Werte aus der App. Das Gerüst ist eine HTML-Vorlage (Template), die im Projektordner unter `src/main/resources` gespeichert wird:

![](images/thymeleaf-new-html-template.png){#id .class max-width=30em}

Eine erste, sehr rudimentäre Vorlage könnte in etwa so aussehen und als `home.html` gespeichert werden:

```html
<!DOCTYPE html>
<html lang="de" xmlns:th="http://www.thymeleaf.org">
    <head>
        <title th:text="${title}">Seitentitel</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Hello <span th:text="${name}">again</span>!</h1>
        <p>Welcome <span th:text="${title}">to our App</span></p>
        <p><span th:text="${result}">Let's see...</span></p>
        <p><span th:text="${goodbye}">CU</span></p>
    </body>
</html>
```

In der Vorlage gibt es fünf Bereiche, in denen Variablen für Thymeleaf stehen, jeweils deklariert durch Attribute im Namespace `th:` nach dem Beispiel  `th:text="${VARIABLENNAME}"` in unterschiedlichen Tags (`<span />`, `<title />`).

Zum Teil sind bereits Platzhalter eingetragen, wie `CU` im Beispiel
`<span th:text="${goodbye}">CU</span>`, die dann später von der App ersetzt werden.


## Einen Controller mit Grundkonfiguration erstellen

Bei der ReST-API, die zuvor implementiert wurde, wurde über die Annotation `@RestController` festgelegt, dass der Rückgabewert der Controller-Methoden unmittelbar an den aufrufenden Client übermittelt werden soll. Das soll hier anders sein: Der _Controller_ stellt das _Model_ zusammen und übergibt nur den Namen der _View_. Die eigentlich auszuliefernde Seite wird anschließend von einem weiteren _Resolver_ zusammengestellt und gesendet.

Für ein erstes "Say-Hello"-Beispiel wird ein Controller benötigt. Dieser kann sinnvollerweise im Ordner der Application selbst liegen (in meinem Beispiel: `src\main\java\de\csbme\IFAXX\myAddressBookApp`).

![Einen Controller erstellen](images/thymeleaf-create-say-hello-controller.png){#id .class max-width=30em}

Netbeans bietet dialoggesteuert die Möglichkeit, einen Controller vorfertigen zu lassen.

![Name, Paket und ErrorHandling in Netbeans auswählen](images/thymeleaf-create-say-hello-controller2.png){#id .class max-width=30em}

Der vorgefertigte Controller erklärt schon relativ viel von dem, was man wissen muss und enthält alle wesentlichen Abhängigkeiten:

```java

package de.csbme.IFAXX.myAddressBookApp;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;


@Controller
public class SayHelloController {

    @RequestMapping("/url")
    public String page(Model model) {
        model.addAttribute("attribute", "value");
        return "view.name";
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Error message")
    public void handleError() {}
}
```

## Den Controller anpassen, um bereits die Basisfunktionalitäten umzusetzen

Die Methode `page()` muss wie folgt angepasst werden:

- der Pfad muss im `@RequestMapping` angepasst werden: `@RequestMapping("/sayhello")`

- die Variable und er Wert, der im _Model_ gesetzt werden soll: `model.addAttribute("name", "SpringBootHero");`

- der Rückgabewert muss dem Namen der View entsprechen - also bei oben angelegtem Template `home.html` muss es heißen:   `return "home";`

- bei Bedarf kann die Methode beliebig umbenannt werden, da der korrekte Aufruf über die Annotation sichergestellt ist

```java
@RequestMapping("/sayhello")
public String page(Model model) {
    model.addAttribute("name", "SpringBootHero");
    return "home";
}
```

## Platzhalter aus unterschiedlichen Quellen ersetzen

Im Template oben gab es vier Variablen, die gesetzt werden können, diese sollen hier beispielhaft aus unterschiedlichen Quellen belegt werden.

### Ein Wert aus dem Pfad soll übernommen werden:
  - die Annotation muss um den gekennzeichneten Bereich (hier: `{name}`) erweitert werden, in dem sich der Variableninhalt befinden soll: `@RequestMapping("/sayhello/{name}")`

  - Als Parameter muss die mit `@PathVariable` annotierte Variable zur Aufnahme des Werts deklariert werden. Spring weist ihr den oben in der URL gekennzeichneten Wert zu.

  - Schließlich muss sie noch an das _Model_ übergeben werden: `model.addAttribute("name", name);`

Im Ganzen:

```java
@RequestMapping("/sayhello/{name}")
 public String page(@PathVariable String name, Model model) {
     model.addAttribute("name", name);
     return "home";
 }
```

![](images/thymeleaf-say-hello-hannes.png){#id .class max-width=30em}

Thymeleaf ersetzt somit den mit  mit `<span th:text="${name}">...</span>` gekennzeichneten Bereich:

```html
<h1>Hello <span th:text="${name}">again</span>!</h1>
```

durch

```html
<h1>Hello <span>Hannes</span>!</h1>
```

## Ein Wert im Request übernehmen

Wenn ein Wert aus Formularen o.ä. übernommen werden soll, der per Request übergeben wird (bei _GET_ z.B. "http://127.0.0.1:8085/sayhello/Hannes?result=DIESERWERT") sind folgende Schritte nötig:

  - In der Methodendeklaration muss ein weiterer annotierter Paramter angegeben und mit Namen versehen werden: `@RequestParam(name = "result") String result`

  - Dieser Parameter wird genutzt, um einen weiteren Wert des _Model_ zu setzen: `model.addAttribute("result", result);`

  Im Ganzen:
```java
  @RequestMapping("/sayhello/{name}")
public String page(@PathVariable String name,
        @RequestParam(name = "result") String result,
        Model model) {
    model.addAttribute("name", name);
    model.addAttribute("result", result);
    return "home";
}
```

Die Ausgabe von http://127.0.0.1:8085/sayhello/Hannes?result=das+klappt+ja+super sollte also etwa so aussehen:

  ![](images/thymeleaf-say-hello-hannes-mit-requestparameter.png)

Thymeleaf ersetzt somit den mit  mit `${result}` gekennzeichneten Bereich:

```html
<p><span th:text="${result}">Let's see...</span></p>
```

ersetzt durch:

```html
<p><span>Das klappt ja super</span></p>
```


## Ein Wert aus den Application Properties übernehmen

In der Datei `src/main/resources/application.properties` hatten wir einen zusätzlichen Wert angegeben, den wir jetzt einbinden wollen:

```ini
spring.application.name=Mein Adressbuch
```

Auf diesen Wert können wir über ein neues Attribut der _Controller_-Klasse zugreifen, wenn diese mit `@Value(...)` annotiert wird (den Wert injeziert dann das Framework):

```java
@Value("${spring.application.name}")
private String title;
```

Dieses Attribut wird an das Model übergeben mit `model.addAttribute("title", this.title);`

Insgesamt sähen die veränderten Bereichs so aus:

```java
// Beispiel-URL: http://127.0.0.1:8085/sayhello/Hannes?result=jippiiiieee
@RequestMapping("/sayhello/{name}")
public String page(@PathVariable String name,
        @RequestParam(name = "result") String result,
        Model model) {
    model.addAttribute("title", this.title);
    model.addAttribute("name", name);
    model.addAttribute("result", result);
    model.addAttribute("goodbye", "See you next time!");
    return "home";
}
```
Die Ausgabe von http://127.0.0.1:8085/sayhello/Hannes?result=das+klappt+ja+super sollte also etwa so aussehen:

  ![](images/thymeleaf-say-hello-hannes-mit-allen-werten.png)

Thymeleaf ersetzt somit die beiden mit  mit `${title}` gekennzeichneten Bereiche:

```html
<title th:text="${title}">Seitentitel</title>
...
<p>Welcome <span th:text="${title}">to our App</span></p>
```

mit den Werten, die in `application.properties` stehen:

```html
<title>Mein Adressbuch</title>
...
<p>Welcome <span>Mein Adressbuch</span></p>
```

## Statische Inhalte (Bilder, CSS, Javascript) einbinden

Wenn nichts anderes konfiguriert wurde werden statische Inhalte für Websites von Spring unter `src\main\resources\static\` gesucht.

Beispielsweise kann eine minimale CSS-Datei unter `src\main\resources\static\stylesheet.css` gespeichert werden:

```css
body {
  padding-top: 5rem;
  background: skyblue;
  background: linear-gradient(white, skyblue);
}

h1 {
	color:#0000FF;
}
```

Eingebunden wird diese im Template innerhalb des `<head>...</head>` Elements mit Hilfe von:

```html
<link rel="stylesheet" th:href="@{/css/stylesheet.css}"/>
```

Thymeleaf liefert dadurch den folgenden HTML-Code an den Client:

```html
<link rel="stylesheet" href="/css/stylesheet.css"/>
```

Gleiches gilt für Bilder der Javascript. Wird ein Bild eingebunden mit

```html
<img th:src="@{/images/logo.png}"/>
```

so muss es im Projektverzeichnis gespeichert werden unter `src\main\resources\static\images\logo.png`. Thymeleaf senden an den Client:

```html
<img src="/images/logo.png"/>
```

Wie so häufig bei Spring können auch diese Pfadangaben konfiguriert werden.

[Die weiteren Schritte finden sich in diesem Tutorial (link)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/05_FormulareNutzen.html)


## Links und weitere Informationen

- [Dokumentation von Thymeleaf](https://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html#introducing-thymeleaf)

- [Mkyong: Spring Boot Hello World Example – Thymeleaf](https://mkyong.com/spring-boot/spring-boot-hello-world-example-thymeleaf/)

- [Eine gute deutschsprachige Einführung zu Thymeleaf und Spring bietet codeflow.site](https://www.codeflow.site/de/article/thymeleaf-in-spring-mvc)

- [SpringBoot-Anleitung von Torsten Horn](https://www.torsten-horn.de/techdocs/Spring-Boot.html#SpringBoot-Thymeleaf)

- [Ein weiteres Tutorial von stackabuse.com](https://stackabuse.com/getting-started-with-thymeleaf-in-java-and-spring/)

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/java-springboot/addressbookapp](https://gitlab.com/oer-informatik/java-springboot/addressbookapp).

Sie sind bei Namensnennung (Hannes Stein) zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
