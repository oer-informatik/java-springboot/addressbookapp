# Automatisches Testen des Modells und Repositories

[Dieser Text ist Bestandteil eines Tutorials zur Erstellung einer einfachen AdressbuchApp mit SpringBoot/Java](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

Mit dem Modell und Repository haben wir eine Struktur angelegt, die zunächst recht abstrakt erscheint. Da es ohnehin nötig ist, für diese Klassen Tests zu generieren, nutzen wir diese, um etwas vertrauter zu werden mit unseren beiden neuen Klassen.

Wir testen die `@Entity`-Klasse `Address` nicht allein, sondern bereits integriert mit dem `AddressRepository`. Um einen Überblick zu erhalten, welche Methoden wir testen sollten, werfen wir nochmal einen Blick auf das Klassendiagramm des Repositories:

![](plantuml/AddressRepository.png)

Es sollten folgende Tests generiert werden:

- jedes Attribut der Entity sollte einmal gelesen und geschrieben werden: `id`, `firstName`, `lastName`

- im CrudRepoistory sollten wir **C**reate/**R**ead/**U**pdate/**D**elete jeweils einmal testen,

    - create: `save(new Address())`

    - read: `findById()`

    - update: `save(existingAddress)`

    - delete: `delete(existingAddress`)`

- Darüber hinaus könnte noch interessant sein:

    - existsById()

    - findById()

    - count()

Das händische Testen ist wichtig, aber umständlich. Künftig sollte dieser Test automatisch erfolgen. Dazu muss zunächst eine jUnit-Testklasse erzeugt werden unter `New`/`File`:

![](images/12_TestAddressController.png)

Nachdem unter `Browse` die zu testende Klasse (AdressRepository) ausgewählt wurde...

![](images/07_TestRepository.png)

...erhalten wir eine vorgefertigte Testklasse. Zunächst müssen wir die Integration in das Spring-Framework mit `@ExtendWith(SpringExtension.class)` ergänzen und die Testmöglichkeiten der JPA mit `@DataJpaTest` aktivieren. D
über die beiden Annotationen an der Klasse ergänzen:


```java
@DataJpaTest
@ExtendWith(SpringExtension.class)
public class AddressRepositoryTest {
```

Wir nutzen für die Tests den `TestEntityManager` (für die erwarteten Ergebnisse) und das `AddressRepository` (für die tatsächlichen Ergebnisse) - von beidem werden per `@Autowired` automatisch konfigurierte Instanzen verknüpft.

```java
public class AddressRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AddressRepository addressRepository;
```

Es wurden vier Methodenrümpfe automatisch erzeugt:

* `@BeforeAll setUpClass(){}`,

* `@AfterAll tearDownClass(){}`,

* `@BeforeEach setUp(){}`,

* `@AfterEach tearDown(){}`

Sie dienen zur Durchführung von Aktionen vor bzw. nach allen Test (`@BeforeAll` / `@AfterAll`) bzw. vor bzw. nach jedem einzelnen Test (`@BeforeEach` / `@AfterEach`). Der zeit benötigen wir diese Funktionalität nicht, wir können sie daher leer belassen (oder sogar löschen).

## Aufbau der einzelnen Testmethode

Jede Testmethode wird mit `@Test` annotiert, damit jUnit sie findet und im Rahmen der Tests ausführt. Sie besteht aus drei Operationen, die explizit in Abschnitten ausgewiesen sein können, oder in einer Operation zusammengefasst werden können:

* das erwartetes Ergebnis festlegen,

* das tatsächliche Ergebnis bestimmen,

* beide vergleichen und das Testresultat ausgeben.

Die Lesbarkeit von Tests erhöht sich deutlich, wenn man diese drei Operationen in jeder Testmethode klar gegliedert implementiert:


### Test-Vorbereitung (_Preparation_):

Zweierlei Dinge werden hier vorbereitet:

  * Das erwartete Ergebnis wird erzeugt / festgelegt.
  * Die Vorbedingungen für den Aufruf der zu testenden Methode werden erfüllt: die nötigen Parameter und die _gemockte_ Instanzen werden erstellt.

Je nach Testumfang kann dieser Abschnitt relativ groß werden. Er wird häufig mit `\\given` markiert.

### Test-Ausführung (_Execution_):

Hier findet lediglich der eigentliche Aufruf der getesteten Methode statt. Soweit nötig wird der Rückgabewert ermittelt. An diesem Abschnitt kann man leicht erkennen, was dieser Test überprüft. Dieser Bereich umfasst häufig nur eine Zeile und kann zur Verdeutlichung mit  `\\when` markiert werden.

### Nachweis des Ergebnisses (_Verification_):

Mit Hilfe von Zusicherungsmethoden (`assert*()`) wird das erwartete Ergebnis mit dem tatsächlichen Ergebnis der zu testenden Methode verglichen. Es kann eine oder mehrere Zusicherungen abgefragt werden. Es ist verbreitet, diesen Abschnitt mit `\\then` zu markieren.

### Beispielhafter Aufbau

```java
@Test
public void testXY() {
          //given
          Object expResult = 1;

          //when
          Object result = testObjekt.getesteteMethode();

          //then
          assertThat(result).isEqualTo(expResult);
        }
```

## `Create`-Test

Um die Erzeugung und das Speichern einer Adresse testen zu können wird zunächst eine Address-Instanz erzeugt...

```java
      Address testAddress = new Address(firstName, lastName);
```

... durch das Repository gespeichert...
```java
     addressRepository.save(testAddress);
```

... und mit dem `TestEntityManager` überprüft, ob diese `Address`-Instanz die selbe ist, wie die gespeicherte Instanz.

```java
Address expResult = testAddress;
Address result = entityManager.find(Address.class, addressId);
assertThat(result).isEqualTo(expResult);
```

Die Klasse `TestEntityManager` bietet eine Reihe von Methoden, mit denen wir unsere Persistenzschicht ansprechen können, ohne direkt auf das Repository zuzugreifen. Einige werden wir später noch benötigen, daher hier ein kleiner Auszug aus dem zugehörigen [JavaDoc](http://127.0.0.1:8082/resource/jar%3Afile%3A/C%3A/Users/hanne/.m2/repository/org/springframework/boot/spring-boot-test-autoconfigure/2.3.3.RELEASE/spring-boot-test-autoconfigure-2.3.3.RELEASE-javadoc.jar%21/org/springframework/boot/test/autoconfigure/orm/jpa/TestEntityManager.html)

![](plantuml/TestEntityManager.png)

Im ganzen könnte der Test z.B. so aussehen:

```java
@Test
 public void testCreateAddress() {
     // given
     System.out.println("Teste das Erzeugen und Speichern von Adresseinträgen");
     String firstName = "Hannes";
     String lastName = "Stein";
     Address testAddress = new Address(firstName, lastName);
     System.out.println("Folgendes Objekt wird gespeichert: " + testAddress);

     // when
     addressRepository.save(testAddress);
     long addressId = testAddress.getId();
     System.out.println("Vergebene ID: " + addressId);
     System.out.println("Danach im RePo vorhanden: " + addressRepository.findAll());

     // then
     Address expResult = testAddress;
     Address result = entityManager.find(Address.class, addressId);
     assertThat(result).isEqualTo(expResult);
 }
 ```

## `Read`-Tests

 Um den Lesevorgang zu testen benutzen wir die  Methode `findById()` des Repositories. Diese liefert ein Objekt der Klasse `Optional<Address>` (also ein generisches Objekt, dass auch `NULL` sein kann).

 ![](plantuml/Optional.png)

 Um diesem Optional das `Address`-Objekt zu entlocken, müssen wir die `get()` Methode aufrufen. Darüber hinaus kann beim Testen die Methode `isPresent()` interessant sein, die prüft, ob ein Objekt existiert.

 Es muss zunächst eine Address-Instanz erzeugt werden...

 ```java
  Address testAddress = new Address("Lars", "Liesmich");
 ```

 ... durch den `entityManager` gespeichert...

 ```java
  long addressId = entityManager.persistAndGetId(testAddress, Long.class);
  entityManager.flush();
 ```

 ...und geprüft, ob das mit dem Repository gelesen Objekt dem entspricht, was wir zu lesen glauben:

 ```java
 Address expResult = testAddress;
 Address result = addressRepository.findById(addressId).get();
 assertThat(result).isEqualTo(expResult);
 assertThat(result.getFirstName()).isEqualTo("Lars");
 ```

 Beispielhaft könnte das im Ganzen so aussehen:

 ```java
 @Test
 public void testReadAddress() {
     // given
     System.out.println("Teste das Lesen von Adresseinträgen");
     String firstName = "Lars";
     String lastName = "Liesmich";
     Address testAddress = new Address(firstName, lastName);

     //when
     long addressId = entityManager.persistAndGetId(testAddress, Long.class);
     entityManager.flush();
     System.out.println("Folgendes Objekt wird gespeichert: " + testAddress);
     System.out.println("Vergebene ID: " + addressId);
     System.out.println("Danach im RePo vorhanden: " + addressRepository.findAll());

     // then
     Address expResult = testAddress;
     Address result = addressRepository.findById(addressId).get();

     assertThat(result).isEqualTo(expResult);
     assertThat(result.getFirstName()).isEqualTo("Lars");
 }
 ```

## `Update`-Test

 Der Ablauf ähnelt dem des `create`-Tests: Es wird eine Address-Instanz erzeugt:

 ```java
Address testAddress = new Address(firstName, lastName);
 ```

 ... durch den `EntityManager` gespeichert und die `id` ausgelesen...

 ```java
long addressId = entityManager.persistAndGetId(testAddress, Long.class);
 ```
...die Attribute werden verändert...

```java
testAddress.setFirstName("Stefan");
testAddress.setLastName("Beispiel");
 ```

 ...über das Repository gespeichert...
```java
addressRepository.save(testAddress);
```     
... und erneut geprüft, ob das erwartete Ergebnis dem gespeicherten entspricht:

```java
Address expResult = testAddress;
Address result = entityManager.find(Address.class, addressId);
assertThat(result).isEqualTo(expResult);
```
Beispielhaft könnte das so aussehen:

```java
@Test
public void testUpdateAddress() {
    // given
    System.out.println("Teste das Aktualisieren von Adresseinträgen");
    String firstName = "Martin";
    String lastName = "Müller";
    Address testAddress = new Address(firstName, lastName);
    System.out.println("Folgendes Objekt wird gespeichert: " + testAddress);
    long addressId = entityManager.persistAndGetId(testAddress, Long.class);
    entityManager.flush();
    System.out.println("Vergebene ID: " + addressId);
    System.out.println("Danach im RePo vorhanden: " + addressRepository.findAll());

    // when
    testAddress.setFirstName("Stefan");
    testAddress.setLastName("Beispiel");
    addressRepository.save(testAddress);

    // then
    Address expResult = testAddress;
    Address result = entityManager.find(Address.class, addressId);

    assertThat(result).isEqualTo(expResult);
}
 ```

## `Delete`-Test

 Auch hier kann ähnlich vorgegangen werden: Es wird eine Address-Instanz erzeugt:

 ```java
Address testAddress = new Address("Lisa", "Löschmich");
 ```

 ... durch den `entityManager` gespeichert...

 ```java
long addressId = entityManager.persistAndGetId(testAddress, Long.class);
entityManager.flush();
 ```

 ...wir überprüfen, ob es ein Objekt mit der zugeordneten ID gibt und ob dies dem ursprünglichen Objekt entspricht:

 ```java
assertThat(entityManager.find(Address.class, addressId)).isEqualTo(testAddress);
 ```

 ...wir löschen die betreffende `Address`-Instanz mit Hilfe der Repository-Methode...

 ```java
addressRepository.delete(testAddress);
 ```

 ... und prüfen schließlich, ob im Repository wirklich kein Objekt mehr mit der `id` verfügbar ist:

 ```java
assertThat(entityManager.find(Address.class, addressId)).isNull();
 ```

 Beispielhaft könnte das so aussehen:

```java
@Test
public void testDeleteAddress() {
   // given
   System.out.println("Teste das Löschen von vorhandenen Adresseinträgen");
   entityManager.persist(new Address("Dagmar", "Düsentrieb"));

   Address testAddress = new Address("Lisa", "Löschmich");
   //when
   long addressId = entityManager.persistAndGetId(testAddress, Long.class);
   entityManager.flush();
   System.out.println("Folgendes Objekt wird gespeichert: " + testAddress);
   System.out.println("Vergebene ID: " + addressId);
   System.out.println("Danach im RePo vorhanden: " + addressRepository.findAll());

   //then
   assertThat(entityManager.find(Address.class, addressId)).isEqualTo(testAddress);

   // when
   addressRepository.delete(testAddress);
   System.out.println("Nach löschen vorhanden: " + addressRepository.findAll());

   // then
   assertThat(entityManager.find(Address.class, addressId)).isNull();
}
```

## Überblick über die Testklasse

Im Ganzen sieht die Klasse mit vier implementierten und vier nicht implementierten Methoden etwa so aus:

![](plantuml/AddressRepositoryTest.png)

## Nächste Schritte

Modell und das Repository können noch nicht per Webrequest angesprochen werden. Um das zu erstellen muss als nächstes ein Controller die HTTP-Endpunkt für Create, Read, Update und Delete anbieten: POST, GET, PUT, DELETE.

[Die weiteren Schritte finden sich in diesem Tutorial (link)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/02_DerKontroller.html)

## Links und weitere Informationen

## _Quellen und offene Ressourcen (OER)_
Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/java-springboot/addressbookapp]() und sind zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
