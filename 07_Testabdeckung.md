# Verbesserung der Testgüte mit JaCoCo

[Dieser Text ist Bestandteil eines Tutorials zur Erstellung einer einfachen AdressbuchApp mit SpringBoot/Java](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

## Installation des JaCoCo-Plugins

Um das Plugin _JaCoCo_ (für Java Code Coverage) nutzen zu können, müssen zwei Anpassungen in der `pom.xml` vorgenommen werden:

* das Plugin selbst muss in dem Abschnitt Plugins ergänzt werden, also
`<project><build><plugins>__HIER__</plugins></build></project>`. Die nötigen Konfigurationabschnitte sind unten gelistet

* dem Spring-boot-maven-plugin muss noch mitgeteilt werden, mit welchen Parametern Jacoco ausgeführt werden soll. Die Argumente für die JVM werden mit der Konfigurationszeile `<jvmArguments>${argLine}</jvmArguments>` übergeben.

Unter `<project><build><plugins>` muss eingefügt werden:

```xml
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.6</version>
    <executions>
        <execution>
            <id>default-prepare-agent</id>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <execution>
            <id>default-report</id>
            <phase>verify</phase>
            <goals>
                <goal>report</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

Zusätzlich muss noch das plugin "spring-boot-maven-plugin" konfiguriert werden. Ein Eintrag existiert bereits, dieser muss nur um die folgenden Einträge ergänzt / mit ihnen ersetzt werden:

```xml
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <configuration>
        <jvmArguments>${argLine}</jvmArguments>
    </configuration>
    <executions>
        <execution>
            <id>start-spring-boot</id>
            <phase>pre-integration-test</phase>
            <goals>
                <goal>start</goal>
            </goals>
        </execution>
        <execution>
            <id>stop-spring-boot</id>
            <phase>post-integration-test</phase>
            <goals>
                <goal>stop</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

## Ausführen eines Testlaufs

Wenn diese beiden Abhängigkeiten übernommen wurden solle ein neuerlicher Testlauf zunächst zu bekanntem jUnit-Ergebnis führen.

![jUnit-Ergebnisse](images/junit-ergebnis.png){#id .class max-height=30vh}

100% bestandene Tests sieht natürlich zunächst ganz erfreulich aus, aber wie aussagefähig sind die Tests?

Wenn die obigen Konfigurationen erfolgreich waren, dann sollte im "File"-Baum unter `target/site/jacoco` eine `index.html`-Datei liegen, die die Testergebnisse enthält. Fehlt diese Datei, dann hilft vielleicht ein "Clean and build Project" im "Run"-Menü oder ein "Build with Dependencies".


![Screenshot des ersten Durchlaufs von JaCoCo](images/jacoco-index.png){#id .class max-height=30em}

Die erstellten Jacoco-Berichte lassen sich im Browser öffnen (beispielsweise über das Kontextmenü und "View")

![Kontextmenü der Jacoco index.html-Datei mit Eintrag "View"](images/jacoco-contextmenu-view.png){#id .class max-height=30%}


## Die Auswertung von Jacoco

Das Ergebnis sieht dann etwa folgendermaßen aus. Auf der Übersichtsseite finden sich die aufsummierten Werte der einzelnen Packages:

![Screenshot des ersten Durchlaufs von JaCoCo](images/jacoco-website-00.png)

Es wird dort ausgegeben:^[Genaue Angaben über die Spalten finden sich in der Dokumentation unter [https://www.jacoco.org/jacoco/trunk/doc/counters.html](https://www.jacoco.org/jacoco/trunk/doc/counters.html)]

* Element Missed Instructions Cov.: **Anweisungsüberdeckungsgrad (C0)**

* Missed Branches Cov.: **Zweigüberdeckungsgrad (C1)**

* Missed Cxty: **Zyklomatische Komplexität**

* Missed Lines: **Zeilenüberdeckungsgrad**

* Missed Methods : **Methodenüberdeckung**:

* Missed Classes: **Klassenüberdeckung**

Im Einzelnen:

### Anweisungsüberdeckung (C0)

Der Anweisungsüberdeckungsgrad gibt an, wie viele einzelne Anweisungen ausgeführt wurden.

Der Überdeckungsgrad wird definiert als:

$$ C_0=  \frac{überdeckte Anweisungen}{alle Anweisungen im Programm}$$ {#eq:c0}


Vorteil:

* findet ggf. „toten Code“ (nicht erreichbare Anweisungen)

Nachteil:

* keine Unterscheidung nach Wichtigkeit von Codezeilen

* Leere Zweige werden nicht gefunden

* Code gilt bereits nach einem Durchlauf als durchlaufen, auch wenn bei weiteren Durchläufen geänderte Daten vorliegen

Anweisungsüberdeckung wird auch Statement Coverage  oder Knotenüberdeckung genannt.

### Zweigüberdeckung (C1)

Der Zweigüberdeckungsgrad gibt an, wie viele Kanten von Verzweigungen des Kontrollflusses durchlaufen werden.

Der Überdeckungsgrad wird definiert als:

$$ C_1=  \frac{überdeckte\ Zweige}{alle\ Zweige\ im\ Programm}$$ {#eq:C1}

Oft werden nur diejenigen Zweige betrachtet, die nicht von anderen Zweigen abhängen (primitiv sind):

$$ C_{1, primitiv} =  \frac{überdeckte\ primitive\ Zweige}{alle\ primitiven\ Zweige\ im\ Programm}$$ {#eq:C1primitiv}


Vorteil der Zweigüberdeckung:

* deckt nicht erreichbare Zweige auf

Nachteile:

* Abhängigkeiten zwischen Zweigen werden nicht getestet (Dazu würde Pfadüberdeckung (C2) benötigt)

* Weder Kombinationen von Bedingungen/Verzweigungen noch komplexere Zustände (Dateien, Betriebssystem) können einbezogen werden (Dazu würde Bedingungsüberdeckung (C3) benötigt);

* Schleifen werden nicht systematisch getestet, da sie nur einmal durchlaufen werden müssen.

Die Zweigüberdeckung wird oft auch Branch Coverage, Kantenüberdeckung oder Edge Coverage genannt.

## Zyklomatische Komplexität

Die _Zyklomatische Komplexität_ stellt ein Maß für die  Wartbarkeit und Testbarkeit von Code dar. Die Zahl repräsentiert die Anzahl (linear) unabhängiger Pfade, entlang derer ein Codeblock durchschritten werden kann. Eine hohe _Zyklomatische Komplexität_ bedeutet, dass zumeist auch viele Testfälle nötig sind und der Code schlecht erweiterbar und wartbar ist. Im Idealfall liegt die Zahl unter 10 - bei Methoden mit einer _Zyklomatischen Komplexität_ von über 50 ist ein Refaktoring unbedingt angezeigt (oder nur mit starken Begründungen zu verwerfen).

Die Zyklomatische Zahl `M` (auch McCabe-Zahl genannt) wird errechnet durch:

$$ M = e - n + 2p$$ {#eq:McCabeZahl}


Wobei :

* `e` für die Anzahl an Kanten (_edges_) steht

* `n` für die Anzahl an Knoten (_nodes_) steht

* `p` steht für "Zusammenhangskomponenten" - in der Regel ist diese Zahl 1; bei parallelen Prozessen oder unabhängigen Pfaden durch Code müsste diese Zahl angepasst werden.

### Zeilenüberdeckungsgrad

Wieviele  Quellcode-Zeilen wurden vom Test ausgeführt? Es sind nicht die Anweisungen, sondern die Programmzeilen relevant, z.B. bei if-Statements kommt es auf die Formatierung an (einzeilig oder mehrzeilig). Der Vorteil ist: Debugger liefert i.d.R. die Zahl der durchlaufenen Zeilen, daher einfach umzusetzen. Diese Kennziffer ist allerdings weniger aussagekräftig als der Anweisungsüberdeckungsgrad.

### Methodenüberdeckung

Wie viele Methoden wurden von keinem Test aufgerufen. Diese Zahl ist unabhängig von der jeweiligen Anweisungs- und Zweigüberdeckung - und daher wenig aussagekräftig für die Testqualität.

### Klassenüberdeckung

Wie viele Klassen wurden von keinem der Tests genutzt. Analog zur Methodenüberdeckung kann diese Zahl eher dazu dienen systematisch bislang ungetestete Codesequenzen zu finden. Als Maß der Codegüte dient sie nicht, da klassen hier als "getestet" erscheinen, sobald der Test auch nur eine Codezeile darin ausführt.

## Ansichten der Ergebnisse

Neben der Auflistung für alle Packages gibt es auch noch die Ansicht für Klassen:

![](images/jacoco-tested-classes.png)

...und zugehörige Methoden:

![](images/jacoco-tested-methods.png)

Auch auf Quelltextebene lässt es sich ausgeben:

![](images/jacoco-tested-code.png)

Auf Basis der so gewonnenen Erkenntnisse können weitere sinnvolle Testfälle ermittelt werden - oder toter Code entfernt werden.

Es gibt kein allgemeingültiges Mindestmaß an Codeabdeckung. Die jeweils erforderliche Testabdeckung sollte in Anbetracht des Anwendungsfalls nach einer Risikoabschätzung sowie der eigenen Fähigkeiten getroffen werden. Einen guten Rahmen gibt die _Testivus_ -Geschichte ["How much unit test coverage do you need"](https://www.artima.com/weblogs/viewpost.jsp?thread=204677) von Alberto Savoia wieder.


## Links und weitere Informationen

- [Jacoco-Einführung von Baeldung](https://www.baeldung.com/jacoco)
- [Jacoco-Dokumentation](https://www.jacoco.org/jacoco/trunk/doc/)

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/java-springboot/addressbookapp](https://gitlab.com/oer-informatik/java-springboot/addressbookapp).

Sie sind bei Namensnennung (Hannes Stein) zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
