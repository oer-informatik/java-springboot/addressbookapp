# Automatisches Testen des Controllers

[Dieser Text ist Bestandteil eines Tutorials zur Erstellung einer einfachen AdressbuchApp mit SpringBoot/Java](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

Der soeben erstellte Controller soll nun um isolierte Unit-Tests ergänzt werden. Es wird hierbei im ersten Schritt nur getestet, ob die Requests den korrekten Controller erreichen und dieser die korrekten Methoden des Repositories aufruft. Da dieser Test nur den Controller und nicht die Datenhaltungsschicht betreffen soll, werden an Stelle der Objekte für die Datenspeicherung (v.a. das Repository) Doubletten gestellt, die deren Funktion nachahmen (_mocken_).

 Um einen Überblick zu erhalten, welche Methoden wir testen sollten, werfen wir nochmal einen Blick auf das Klassendiagramm des Repositories:

 ![](plantuml/AddressController.png)

Es sollten für folgende Methoden Tests generiert werden:

- die vier CRUD-Methoden (**C**reate/**R**ead/**U**pdate/**D**elete):

    - create: `post(new Address())`

    - read: `get(id)`

    - update: `put(address)`

    - delete: `delete(id)`

- Darüber hinaus gibt es noch:

    - `list()`

## Erstellen einer Test-Klasse

Zunächst muss wieder eine jUnit-Testklasse erzeugt werden. Die IDE Netbeans unterstützt das beispielsweise unter `New`/`File`:

![](images/12_TestAddressController.png)

Nachdem unter _Browse_ die zu testende Klasse (`AddressController`) ausgewählt wurde erhalten wir eine vorgefertigte Testklasse mit einer Testmethode für jede zu testende Methode der Ausgangsklasse.

![](images/12_TestAddressController2.png)

Zunächst müssen wir die jUnit5-Integration in das Spring-Framework mit `@ExtendWith(SpringExtension.class)` ergänzen und die Testmöglichkeiten des MVC-Frameworks mit `@WebMvcTest` aktivieren. Daher müssen diese beiden Annotationen an der Klasse ergänzt werden:

```java
@ExtendWith(SpringExtension.class)
@WebMvcTest(
  includeFilters = @ComponentScan.Filter(
    type = FilterType.ASSIGNABLE_TYPE,
    classes=AddressController.class
  ))
public class AddressControllerTest {...}
```

Es werden für jede existierende Methode des Controllers Testmethoden erstellt. Diese scheitern zunächst alle in der Voreinstellung (Menü Run/Test oder `Alt`-`F6`):

![](images/12_TestResults.png)

### Mocken des Repositories

Als Voraussetzung für den Test benötigen wir ein Umfeld (einen "Kontext"), in dem unsere HTTP-Requests aufgerufen werden. Über die Annotation `@WebMvcTest` verknüpfen wir dieses Umfeld. Dieser Kontext soll auf fingierte HTTP-Aufrufe reagieren. Das wiederum übernimmt ein Objekt der Klasse `MockMvc`. Über die Annotation `@Autowired` weiß das SpringBoot-Framework, dass es selbst die zugehörigen Instanzen dieser Klassen erzeugen und verknüpfen muss.

```java
@Autowired
private MockMvc mockMvc;

@MockBean
private AddressRepository addressRepository;
```

Um den Controller unabhängig von dem Repository und Model testen zu können, muss das Repository _gemockt_ werden - also ein Testdouble erstellt werden, was keinerlei Logik enthält, sondern einfach bei Aufruf einer Methode einen zuvor definierten Rückgabewert übergibt. Hierzu verwenden wir das Tool _Mockito_. Dass wir das Repository simulieren wollen, hatten wir ja bereits über `@MockBean private AddressRepository addressRepository;` festgelegt.

### Funktionsaufrufe nachahmen mit Mockito

Wie auf einen Funktionsaufruf reagiert werden soll können wir mit der Mockito-Methode `when()` festlegen (die unglücklicherweise so heißt wie unser `given/when/then`-Bereich in den Testmethoden, mit diesem aber nichts zu tun hat):

```java
when(AUFGERUFENE_METHODE).thenReturn(RÜCKGABEWERT);
```

Die aufgerufene Methode kann hierbei direkt mit konkreten Parameterwerten genannt werden:

```java
when(quadriere(2L)).thenReturn(4L);
```

Wenn wir keine konkreten Parameter mocken wollen, sondern bei jedem Methodenaufruf den selben Rückgabewert simulieren wollen. kann dies mit `any(KLASSENOBJEKT)` angegeben werden. Konkret sieht das so aus:

```java
when(addressRepository.save(any(Address.class))).thenReturn(testAddress);
```

## Die einzelnen Testmethoden

Wir ergänzen für jede HTTP-Request-Methode einen ersten Test der überprüft, ob der korrekte HTTP-StatusCode, der korrekte Inhaltstyp (JSON) und der korrekte Inhalt zurückgegeben wird.

Hierzu nutzen wir wieder die Aufteilung in `given`, `when` und `then`.

Im folgenden müssen wir den HTTP-Request mocken. Dazu bauen wir einen Request mit dem `MockHttpServletRequestBuilder`, führen diesen mit `perform()` aus und überprüfen die Ergebnisse mit `andExpect()`.

![](plantuml/MockMvc.png)

####  `POST`-Methode

Am Beispiel eines Tests für die `POST`-Methode zur Erzeugung neuer Ressourcen soll der Ablauf eines Tests exemplarisch dargestellt werden:

- `//given`:

    - es wird ein beim Test zu speicherndes `Address`-Objekt erzeugt und als JSON-String serialisiert

    ```java
    Address testAddress = new Address(testFirstName, testLastName);
    String inputJson = mapToJson(testAddress);
    ```

    Um JSON-Objekte zu erstellen wurde eine Methode `mapToJSON` erstellt. Diese muss in die Testklasse eingefügt werden.

    ```java
    protected String mapToJson(Object obj) throws JsonProcessingException {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(obj);
    }
    ```

    - die `save()`-Methode des Repositories wird normalerweise aufgerufen. Diese Methode muss gemockt werden, da wir den Controller hier isoliert testen wollen. Das gemockte Repository soll beim Aufruf von save() immer mit dem `testAdress`-Objekt antworten - unabhängig davon, was als Parameter übergeben wurde:

    ```java
    when(addressRepository.save(any(Address.class))).thenReturn(testAddress);
    ```

- `//when`:

  - Hier wird ein gemockter Request erstellt. Im Gegensatz zu unseren Requests, die wir für die manuellen Tests per PowerShell oder Bash zusammengestellt hatten, lassen wir uns einen Request über das Builder-Pattern bauen.
  In dieser Phase sind im Wesentlichen drei Klassen beteiligt: unsere jUnit-Testklasse, die Klasse `MockMvcRequestBuilders`, die die richtige Builderklasse liefert und vorkonfiguriert und die Builderklasse selbst (`MockHttpServletRequestBuilder`). Im UML-Sequenzdiagramm werden die Rollen der drei Klassen deutlicher:


   ![](plantuml/AddressController-testPOST-1.png)

    - die statische Methode `post()` des `MockMvcRequestBuilders` liefert uns ein passendes Builder-Objekt

    ```java
    MockHttpServletRequestBuilder testRequest = post("/api/address") //[...]
    ```

    ![](plantuml/MockMvcRequestBuilders.png)

    - Der so erzeugte `RequestBuilder` wird nun konfiguriert: _Accept_- und _Content-Type-Header_ des Requests werden festgelegt sowie der eigentliche Request-Body als UTF-8-JSON-String übergeben. Jede dieser Konfigurations-Methoden gibt selbst wieder ein angepasstes `MockHttpServletRequestBuilder`-Objekt zurück, dadurch können sie in dieser Weise verkettet werden (_Method-Chaining_):

    ```java
      .accept(MediaType.APPLICATION_JSON)
      .contentType(MediaType.APPLICATION_JSON)
      .content(inputJson);
    ```


- `//then`:    
    - Der konfigurierte `RequestBuilder` wird als Parameter an `mockMVC.perform()` übergeben.

    ```java
        ResultActions resultAction = mockMvc.perform(testRequest) //[...]
    ```

    In der Methode `perform()` wird das eigentliche `MockHttpServletRequest`-Objekt vom Builder  erstellt, der Request wird ausgeführt und das davon zurückgegebene `response`-Objekt wird gemeinsam mit dem Request in ein Objekt des Typs `MvcResult` gekapselt. Dieses Objekt wird per _Dependency Injection_ in ein neu erstelltes `ResultActions`-Objekt übergeben:

    ![](plantuml/AddressController-testPOST-2.png)

    - als Ergebnis erhalten wir ein `ResultActions`-Objekt, das uns erlaubt das Ergebnis über Methoden-Verkettung detailliert zu untersuchen. Jeder Aufruf von `andExpect()` gibt wiederum ein `ResultActions`-Objekt zurück:

    ```java
    .andExpect(status().isCreated())
    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
    .andExpect(jsonPath("$.firstName", equalTo(testFirstName)))
    .andExpect(jsonPath("$.lastName", equalTo(testLastName)));
    ```

    Diese verketteten Methoden bilden die eigentlichen Zusicherungen, die wir in bisherigen Unit-Tests mit den `assert*(*)`-Methoden umgesetzt hatten. Ein Blick hinter die Kulissen zeigt auch, dass am Ende der Prozesse ein `assertEquals()`-Aufruf steht:

    ![](plantuml/AddressController-testPOST-3.png)

    Es wird zunächst ein `ResultMatcher`-Objekt erzeugt und dieses wird der `andExpect()`-Methode übergeben. `andExpect()` ist eine Methode des `ResultActions`-Objekts des vorigen `perform()` bzw. `andExpect()`-Aufrufs und verfügt als solches bereits über die Abhängigkeit des `MvcResult` (darin enthalten das Request- und Response-Objekt).

    Es ist nicht immer offensichtlich, zu welchen Klassen aufgerufene Methoden gehören, da die IDE einige statische Imports vornimmt. Um beispielsweise herauszufinden, an welcher Stelle `status()`, `andExpect()` oder `isCreaded()` implementiert werden lohnt ein Blick in die Sourcen, in JavaDoc oder in die Zusammenfassung in diesem UML-Klassen-Diagramm:

    ![](plantuml/ResultActions.png)

Im Ganzen sieht diese Test-Methode für `POST` folgendermaßen aus:

```java
@Test
public void testPost() throws Exception {
    //given
    String testFirstName = "Herbert";
    String testLastName = "Testkandidat";
    Address testAddress = new Address(testFirstName, testLastName);
    String inputJson = mapToJson(testAddress);

    //Mocken des Repositories
    when(addressRepository.save(any(Address.class))).thenReturn(testAddress);


    //when
    MockHttpServletRequestBuilder testRequest = post("/api/address")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .content(inputJson);

    // then
    ResultActions result = mockMvc.perform(testRequest)
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.firstName", equalTo(testFirstName)))
            .andExpect(jsonPath("$.lastName", equalTo(testLastName)))
            ;
}
```

Nach diesem Schema werden auch die anderen Tests erstellt.

#### List-Methode

Da die `list()`-Methode des Controllers auf `addressRepository.findAll()` zugreift müssen wir hier beim Mocken ansetzen.

Wir erzeugnen den RequestBuilder über die `get()`-Methode und überprüfen, ob der Statuscode 200 ("OK") ist. Darüber hinaus stellt das JSON-Objekt, dass wir als Antwort erhalten, ein Array dar, deshalb müssen wir bei der Inhaltsprüfung die jeweilige Zeile ansprechen: `"$[0].firstName"`

Die sonstige Struktur des `post()`-Tests bleibt weitgehend erhalten.

```java
@Test
 public void testList() throws Exception {
     //given
     String testFirstName = "Herbert";
     String testLastName = "Testkandidat";

     //Mocken des Repositories
     when(addressRepository.findAll()).thenReturn(new ArrayList<Address>(Arrays.asList(new Address(testFirstName,testLastName),new Address(testFirstName,testLastName))));

     //when
     MockHttpServletRequestBuilder testRequest = get("/api/address").accept(MediaType.APPLICATION_JSON);

     // then
     mockMvc.perform(testRequest)
             .andExpect(status().isOk())
             .andExpect(content().contentType(MediaType.APPLICATION_JSON))
             .andExpect(jsonPath("$[0].firstName", equalTo(testFirstName)))
             .andExpect(jsonPath("$[0].lastName", equalTo(testLastName)));
 }
```

 #### GET-Methode

 Auch hier weichen die zu mockende Methode (`findById()`) und der RequestBilder-Aufruf (`get()`) ab:

```java
 @Test
 public void testGet() throws Exception{
     //given
     String testFirstName = "Herbert";
     String testLastName = "Testkandidat";

     //Mocken des Repositories
     when(addressRepository.findById((long) 1)).thenReturn(Optional.of(new Address(testFirstName,testLastName)));

     //when
     MockHttpServletRequestBuilder testRequest = get("/api/address/{id}", 1)
            .accept(MediaType.APPLICATION_JSON);

     // then
     mockMvc.perform(testRequest)
             .andExpect(status().isOk())
             .andExpect(content().contentType(MediaType.APPLICATION_JSON))
             .andExpect(jsonPath("$.firstName", equalTo(testFirstName)))
             .andExpect(jsonPath("$.lastName", equalTo(testLastName)));
 }
```

#### PUT-Methode

Hier müssen zwei Methoden des Repositories gemockt werden:  `save()` und `existsById()`


```java
@Test
 public void testPut() throws Exception {
     //given
     String testFirstName = "Herbert";
     String testLastName = "Testkandidat";
     Long id = 1L;
     Address testAddress = new Address(testFirstName,testLastName);
     testAddress.setId(id);
     String inputJson = mapToJson(testAddress);

     //Mocken des Reposiroties
     when(addressRepository.save(any(Address.class))).thenReturn(testAddress);
     when(addressRepository.existsById(id)).thenReturn(true);

     //when
     MockHttpServletRequestBuilder testRequest = put("/api/address/"+id)
             .accept(MediaType.APPLICATION_JSON)
             .contentType(MediaType.APPLICATION_JSON)
             .content(inputJson);
             ;

     // then
     ResultActions a = mockMvc.perform(testRequest)
             .andExpect(status().is2xxSuccessful())
             .andExpect(content().contentType(MediaType.APPLICATION_JSON))
             .andExpect(jsonPath("$.firstName", equalTo(testFirstName)))
             .andExpect(jsonPath("$.lastName", equalTo(testLastName)))
             ;
 }
```

#### Delete

Bei `delete()` fallen die Tests deutlich schlanker aus, da wir bei Unit-Test zunächst nur den korrekten Aufruf der Repository-Methoden prüfen. Integrationstests wären hier im nächsten Schritt sicherlich angebracht, um das Zusammenspiel von Controller und Datenschicht zu testen.

```java
@Test
public void testDelete() throws Exception {

    //given
    Long id = 1L;
    //Mocken des Reposiroties
    when(addressRepository.existsById(id)).thenReturn(true);

    //when
    MockHttpServletRequestBuilder testRequest = delete("/api/address/{id}", id)
            //.accept(MediaType.APPLICATION_JSON)
            ;

    // then
    mockMvc.perform(testRequest)
            .andExpect(status().is2xxSuccessful());
}
```

### Fazit

Bis hierhin kann allenfalls von einem rudimentären ersten Testfragment gesprochen werden: die eigentlichen Testfälle gemäß der Blackbox-Systematik sowie eine Messung der Testabdeckung mit entsprechender Ergänzung der Testfälle müssen nun folgen.

[Die weiteren Schritte finden sich in diesem Tutorial (link)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/03_ErweiterungenDesModells.html)

## Links und weitere Informationen

## _Quellen und offene Ressourcen (OER)_
Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/java-springboot/addressbookapp]() und sind zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
