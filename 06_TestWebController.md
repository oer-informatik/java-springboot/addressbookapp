# Testen der Formulare mit Thymeleaf und Controller

[Dieser Text ist Bestandteil eines Tutorials zur Erstellung einer einfachen AdressbuchApp mit SpringBoot/Java](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

Zum Erstellen der einzelnen Testfälle hilft zunächst ein Blick auf das zuletzt erstellte Zustandsdiagramm: hier ist festgelegt, welche Methode bei welcher Route mit welcher View  aufgerufen wird. Um das Zusammenspiel von _Controller_ und _View_ zu testen sollte für jede Transition mindestens ein Test erfolgen. Wir hatten ermittelt, dass wir in einem einfachen Oberflächen-Design auf  drei _Views_, sieben Zustände und sechs verschiedene Routen kommen, die es nun zu testen gilt:

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp.png)


## Die zugehörige Testklasse erstellen

Natürlich muss auch der WebController getestet werden. Dazu muss eine neue Testklasse erzeugt werden:

![Neue Testklasse erstellen](images/webcontroller-new-test.png){#id .class max-width=30em}

Die zu testende Klasse auswählen (per Browse):

![Neue Testklasse erstellen](images/webcontroller-new-test-configure.png){#id .class max-width=30em}


Die erzeugte Klasse enthält bereits wieder Dummies für die einzelnen zu testenden Methoden sowie die Methoden zur Konfiguration.


* `@BeforeAll setUpClass(){}`,

* `@AfterAll tearDownClass(){}`,

* `@BeforeEach setUp(){}`,

* `@AfterEach tearDown(){}`

Diese Methoden können ohne Methodenrumpf bleiben, da uns das Framework mit den zuständigen Abhängigkeiten versorgt bzw. wir die übrigen Ressourcen _mocken_ und daher nicht konfigurieren, öffnen oder schließen müssen.


Wir müssen per Annotation die Abhängigkeiten zum Spring-Testframework und zur getesteten Klasse setzen:

```java
@ExtendWith(SpringExtension.class)
@WebMvcTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes=AddressWebControllerTest.class))
public class AddressWebControllerTest {...}
```

## Mocken des Repositories

Das Repository wollen wir ja nicht mit testen, sondern isoliert den neuen Controller und dessen Zusammenspiel mit der _View_. Daher wird festgelegt, dass das Repository gemockt wird. Das Verhalten der gemockten Methoden definieren wir in den einzelnen Testmethoden.

```java
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AddressRepository addressRepository;
```

## Testmethode für _Create_ (_Route_ `POST "/address"`)

Auch die einzelnen Testmethoden können weitgehend von den Tests des ReST-Controllers übernommen werden.

Es bietet sich auch hier an, den Aufbau der Testmethoden zu gliedern:^[den given-when-then-Ansatz erklärt Martin Fowler knapp in einem Artikel unter : [https://martinfowler.com/bliki/GivenWhenThen.html](https://martinfowler.com/bliki/GivenWhenThen.html)]

* _given_:

  - Die Voraussetzungen, bevor der Test beginnt.

  - In diesem Abschnitt wird das zu testende System in den Zustand gebracht, in dem der Test durchgeführt werden kann.

  - Aufrufe an das Framework, die für einen erfolgreichen Test erforderlich sind werden in diesem Abschnitt vorgenommen.

  - konkret: Das Repository wird in den gewünschten Zustand gebracht (über mocken), die benötigten Objekte werden erzeugt...

* _when_:

  - die eigentliche Testspezifikation

  - Aufruf der zu testenden Methode: Welche Aktion soll genau getestet werden?

  - konkret: ein Request auf die im Zustandsdiagramm genannte Route mit erforderlichen Parametern erfolgt

* _then_:

  - der Vergleich des eingetretenen mit dem erwarteten Ergebnis

  - konkret:

    - Ist der HTTP-Statuscode wie erwartet?

    - Wurde die richtige _View_ angefordert?

    - Enthält die Antwort (die befüllte View) die erwarteten Werte?

## Testen der `create()`-Methode (_Route_ `POST "/address"`)


Nach vorgenannter Struktur soll der Test folgendermaßen aufgebaut sein:

* _given_:

  - Für eine neue Beispieladresse ("Herbert Testkandidat")...

  - ... soll unter Umgehung der Logik des Repositories (`save()` und `findAll()` werden _gemockt_)...

* _when_:

  - ...beim Aufruf der Route `POST: /address`...

  - ... mit dem Testobjekt per Parameter übergeben...

* _then_:

  - ...der korrekte Statuscode (`200` / `OK`) zurückgegeben werden,...

  - ...die richtige View (`addresses`) ausgewählt werden und...

  - ... im Antwortstring der Name der Testadresse enthalten sein.

Im Einzelnen sieht das dann so aus (und ist erst mit unten abgedruckter Hilfsmethode lauffähig):

```java
    @Test
    public void testCreate() throws Exception {
        //given
        String testFirstName = "Herbert";
        String testLastName = "Testkandidat";
        Address testAddress = new Address(testFirstName, testLastName);

        //Mocken des Repositories
        when(addressRepository.save(any(Address.class))).thenReturn(testAddress);
        when(addressRepository.findAll()).thenReturn(new ArrayList<Address>(Arrays.asList(new Address(testFirstName,testLastName),new Address(testFirstName,testLastName))));

        //when
        MockHttpServletRequestBuilder testRequest = post("/address");
        Map<String, String> postParams = addressObjectToPostParams(testAddress);
        for (final Entry<String, String> entry : postParams.entrySet()) {
            testRequest.param(entry.getKey(), entry.getValue());
        }

        // then
        ResultActions a = mockMvc.perform(testRequest)
                .andExpect(status().isOk())
                .andExpect(view().name("addresses"))
                .andExpect(content().string(containsString(testFirstName)));
    }
```

## Hilfsmethode addressObjectToPostParams()

Die Parameterübergabe erfolgt im Test oben durch eine HashMap, über die iteriert wird, um sie einzeln als Parameter zu übergeben:

```java
Map<String, String> postParams = addressObjectToPostParams(testAddress);
for (final Entry<String, String> entry : postParams.entrySet()) {
    testRequest.param(entry.getKey(), entry.getValue());
}
```

Es wird über Objekte der Klasse `Entry` iteriert. Hierbei handelt es sich um Key/Value-Paare einer HashMap. Die betreffende Klasse `Entry` müssen wir aus `java.util.Map` importieren:
```java
import java.util.Map.Entry;
```

Dazu muss noch eine Hilfsmethode erstellt werden, die die Attribute des _Model_-Objekts in eine HashMap wandelt:



```java
public static Map<String, String> addressObjectToPostParams(Address address){
        Map<String, String> postMap = new HashMap<String, String>();
        postMap.put("firstName", isNull(address.getFirstName())?"":address.getFirstName());
        postMap.put("lastName", isNull(address.getLastName())?"":address.getLastName());
        postMap.put("id", isNull(address.getId())?"":address.getId().toString());
        return postMap;
    }
}
```

Dazu sind Imports für `isNull()` nötig:

```java
import static java.util.Objects.isNull;
```

## Testmethode für `list()` (_Route_ `GET "/address"`)


Nach vorgenannter Struktur soll der Test folgendermaßen aufgebaut sein:

* _given_:

  - Für eine Liste aus Beispieladressen...

  - ... die unter Umgehung der Logik des Repositories zurückgegeben werden (`findAll()` wird _gemockt_)...

* _when_:

  - ...soll beim Aufruf der Route `GET: /address`...

* _then_:

  - ...der korrekte Statuscode (200 / OK) zurückgegeben werden,...

  - ...die richtige View (`addresses`) ausgewählt werden und...

  - ... im Antwortstring der Name der Testadresse(n) enthalten sein.

```java
    @Test
    public void testList() throws Exception {
        //given
        String testFirstName = "Herbert";
        String testLastName = "Testkandidat";
        Address testAddress = new Address(testFirstName, testLastName);

        //Mocken des Repositories
        when(addressRepository.findAll()).thenReturn(new ArrayList<Address>(Arrays.asList(new Address(testFirstName+"A",testLastName+"A"),new Address(testFirstName,testLastName))));

        //when
        MockHttpServletRequestBuilder testRequest = get("/address");

        // then
        ResultActions a = mockMvc.perform(testRequest)
                .andExpect(status().isOk())
                .andExpect(view().name("addresses"))
                .andExpect(content().string(containsString(testFirstName)));

    }
```

Der Import für `get()` ist erforderlich:

```java
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
```

## Testmethode für `showDetails()` (_Route_ `GET "/address/{id}"`)


Analog zu den beiden vorgenannten Tests ist die Struktur:

* _given_:

  - Für eine Testadresse mit der `id=1`,...

  - ...die unter Umgehung der Logik des Repositories zurückgegeben wird (`findById()` wird _gemockt_)...

* _when_:

  - ...soll beim Aufruf der Route `GET: /address/1`...

* _then_:

  - ...der korrekte Statuscode (200 / OK) zurückgegeben werden,...

  - ...die richtige View (`addressDetail`) ausgewählt werden und...

  - ... im Antwortstring der Name der Testadresse enthalten sein.

```java
    @Test
    public void testShowDetails() throws Exception {
        //given
        String testFirstName = "Herbert";
        String testLastName = "Testkandidat";

        //Mocken des Repositories
        when(addressRepository.findById((long) 1)).thenReturn(Optional.of(new Address(testFirstName,testLastName)));

        //when
        MockHttpServletRequestBuilder testRequest = get("/address/1");

        // then
        ResultActions a = mockMvc.perform(testRequest)
                .andExpect(status().isOk())
                .andExpect(view().name("addressDetail"))
                .andExpect(content().string(containsString(testFirstName)));
    }
```

## Testmethode für editDetails() (_Route_ `GET "/address/edit/{id}"`)



* _given_:

  - Für eine Testadresse mit der `id=1`,...

  - ...die unter Umgehung der Logik des Repositories zurückgegeben wird (`findById()` wird _gemockt_)...

* _when_:

  - ...soll beim Aufruf der Route `GET: /address/edit/1`...

* _then_:

  - ...der korrekte Statuscode (200 / OK) zurückgegeben werden,...

  - ...die richtige View (`addressEdit`) ausgewählt werden und...

  - ... im Antwortstring der Name der Testadresse enthalten sein.

```java
    @Test
    public void testEditDetails() throws Exception {
        //given
        String testFirstName = "Herbert";
        String testLastName = "Testkandidat";
        Long id = 1L;
        Address testAddress = new Address(testFirstName, testLastName);
        testAddress.setId(id);

        //Mocken des Repositories
        when(addressRepository.findById((long) 1)).thenReturn(Optional.of(new Address(testFirstName,testLastName)));

        //when
        MockHttpServletRequestBuilder testRequest = get("/address/edit/{id}", id);

        // then
        ResultActions a = mockMvc.perform(testRequest)
                .andExpect(status().isOk())
                .andExpect(view().name("addressEdit"))
                .andExpect(content().string(containsString(testFirstName)));
    }
```

## Testmethode für update() (_Route_ `POST "/address/{id}"`)


* _given_:

  - Für eine Testadresse mit der `id=1`...

  - ...soll unter Umgehung der Logik zur Speicherung und Auflistung des Repositories (`findAll()`, `save()` und `existsById()` wird _gemockt_)...

* _when_:

  - ...soll beim Aufruf der Route `POST: /address/1`...

  - ... mit einem deänderten Objekt als Parameter...

* _then_:

  - ...der korrekte Statuscode (200 / OK) zurückgegeben werden,...

  - ...die richtige View (`addresses`) ausgewählt werden und...

  - ... im Antwortstring der Name der Testadresse enthalten sein.

```java
    @Test
    public void testUpdate() throws Exception {
        //given
        String testFirstName = "Herbert";
        String testLastName = "Testkandidat";
        Long id = 1L;
        Address testAddress = new Address(testFirstName,testLastName);
        testAddress.setId(id);

        //Mocken des Repositories
        when(addressRepository.save(any(Address.class))).thenReturn(testAddress);
        when(addressRepository.existsById(id)).thenReturn(true);
        when(addressRepository.findAll()).thenReturn(new ArrayList<Address>(Arrays.asList(new Address(testFirstName+"A",testLastName+"A"),new Address(testFirstName,testLastName))));

        //when
        MockHttpServletRequestBuilder testRequest = post("/address/1");
        Map<String, String> postParams = addressObjectToPostParams(testAddress);
        for (final Entry<String, String> entry : postParams.entrySet()) {
            testRequest.param(entry.getKey(), entry.getValue());
        }

        // then
        ResultActions a = mockMvc.perform(testRequest)
                .andExpect(status().isOk())
                .andExpect(view().name("addresses"))
                .andExpect(content().string(containsString(testFirstName)));

    }
```

Der Import für `post()` ist erforderlich:

```java
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
```

## Testmethode für `delete()` (_Route_ `POST "/address/delete/{id}"`)

Auch für die letzte der CRUD-Methoden wird der Unit-Test nach gleichem Muster erstellt. Da hier das Repository selbst nicht getestet werden soll fällt die Logik denkbar kurz aus:

* _given_:

  - Für eine `id=1`, die gelöscht werden soll...

  - ...soll unter Umgehung der Logik zum finden von Einträgen im Repository (`existsById()` wird _gemockt_)...

  - ... ohne, dass auch die Löschfunktion gesondert gemockt werden muss, da sie ohnehin `void` als Rückgabewert hat (was der Mocking-Standard ist)...

* _when_:

  - ...soll beim Aufruf der Route `POST: /address/delete/1`...

* _then_:

  - ...der korrekte Statuscode (200 / OK) zurückgegeben werden,...

  - ...die richtige View (`addresses`) ausgewählt werden und...

```java
    @Test
    public void testDelete() throws Exception {
         //given
        Long id = 1L;

        //Mocken des Reposiroties
        when(addressRepository.existsById(id)).thenReturn(true);

        //when
        MockHttpServletRequestBuilder testRequest = post("/address/delete/{id}", id);

        // then
        mockMvc.perform(testRequest)
                .andExpect(status().isOk())
                .andExpect(view().name("addresses"));
    }
```
## Übersicht der Imports

Nicht immer werden alle Imports zuverlässig gefunden. Um im Einzelfall den Fehler zu finden hier die Gesamtliste der Imports des Controllers:

```java
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import static java.util.Objects.isNull;
import java.util.Optional;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import static org.hamcrest.Matchers.containsString;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.ModelAndView;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
```


## Fehlende Testfälle

Die Anwendung muss noch um weitere Testfälle ergänzt werden. Nicht alle der oben angestrebten Transitionen werden auf diesem Weg getestet. Da bei den weiteren Tests jedoch keine weiteren Techniken mehr erforderlich sind, wird auf eine Nennung in diesem Rahmen verzichtet.

[Die weiteren Schritte finden sich in diesem Tutorial (link)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/07_Testabdeckung.html)

## Links und weitere Informationen

- [Eine gute deutschsprachige Einführung zu Thymeleaf und Spring bietet codeflow.site](https://www.codeflow.site/de/article/thymeleaf-in-spring-mvc)

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/java-springboot/addressbookapp](https://gitlab.com/oer-informatik/java-springboot/addressbookapp).

Sie sind bei Namensnennung (Hannes Stein) zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
