# Erstellen eines einfachen AdressbuchApp mit SpringBoot

## Übersicht über das Tutorial

[Dieser Abschnitt ist Bestandteil eines Tutorials zur Erstellung einer einfachen AdressbuchApp mit SpringBoot/Java](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

- [Initialisierung des SpringBoot-Projekts](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

- [Erste Reaktion: ein klassisches "Hello World"](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00a_HelloWorld.html)

- [Die Datenhaltung: das Modell](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/01_DasModell.html)

- [Testen des Modells mit jUnit](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/01a_AutomatischesTestenDesRepositories.html)

- [Ein Kontroller für eine REST-API erstellen](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/02_DerKontroller.html)

- [Testen des Controllers mit jUnit](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/02a_AutomatischesTestenDesControllers.html)

- [Erweiterung des Modells für Entitätstyp-Beziehungen unterschiedlicher Kardinlaitäten](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/03_ErweiterungenDesModells.html)

- [Die View mit Hilfe der Template-Engine Thymeleaf](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/04_TemplateEngineThymeleaf.html)

- [Einen Controller erstellen für die Thymeleaf-Views](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/05_FormulareNutzen.html)

- [Den neuen Controller mit jUnit testen](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/06_TestWebController.html)

- [Testabdeckung mit JaCoCo messen und Abdeckungsgrad der Tests erhöhen](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/07_Testabdeckung.html)


## Projekt-Initialisierung

In Netbeans ein neues "Java with Maven"-Projekt erstellen (dazu muss SpringBoot installiert sein sowie ggf. Proxy-Einstellungen vorgenommen worden sein.). Die Auswahl "Spring Boot Initializr" nimmt einiges an Arbeit ab:

![](images/00_NewProject.png)

Angabe von Group: `de.csbme.IFAXX`
Artifact: myAddressBookApp
Java-Version: 11

![](images/01_SpringInitializr.png)

Wir werden einige Abhängigkeiten benötigen, die wir später noch in der zuständigen Konfigurationsdatei händisch einfügen. Zunächst genügt hier "Spring Web" (steht ggf. weiter unten unter "Web", wenn es noch nicht benutzt wurde):
![](images/02_UseSpringWeb.png)

Speicherort und Projektnamen eingeben:

![](images/03_NameAndLocation.png)

Wir erhalten so ein neues Projekt, das im Projektbaum wie folgt aussieht (wichtige Datein markiert):

![](images/04_ProjectView.png)

Zur Orientierung: im File-Baum sieht das Projekt wie folgt aus:

![](images/05_FileView.png)

Die zentrale erzeugte Datei sieht wie folgt aus:

```java
package de.csbme.IFAXX.myAddressBookApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyAddressBookAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyAddressBookAppApplication.class, args);
	}

}
```

Von zentraler Bedeutung ist die Annotation `@SpringBootApplication`. Ein Blick in die Sourcen oder JavaDoc klärt darüber auf, was es mit dieser Annotation auf sich hat. Ein Rechtsklick auf die Annotation führt uns im Kontext-Menü zu den JavaDoc-Seiten.

![](images/05_ShowJavaDoc.png)


```java
/*...*/
 @SpringBootConfiguration
 @EnableAutoConfiguration
 @ComponentScan(/*...*/)
public @interface SpringBootApplication

```
Es handelt sich um eine _Composed Annotation_, die folgende Annotations enthält:

- `@SpringBootConfiguration`: Eine Spezialisierung von `@Configuration`, kennzeichnet die zentrale SpringBoot Anwendung

- `@EnableAutoConfiguration`: Konfiguriert Komponenten automatisch

- `@ComponentScan`: Durchsucht alle Klassen in diesem Package (und darunter liegenden Packages) nach der Annotation `@Components` und findet so Konfigurationen, Komponenten (z.B. Controller) und Dienste

![](plantuml/SpringBootApplication.png)

## Nächste Schritte

Als nächstes wird die Schicht zur Datenspeicherung in einer Datenbank erstellt (Persistenzschicht). Hierzu müssen

- Abhängigkeiten ergänzt

- eine Modell-Klasse erstellt werden

- ein Repository zum Handling des Modells erstellt werden

[Die weiteren Schritte finden sich in diesem Tutorial (link)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00a_HelloWorld.html)

## Links und weitere Informationen

## _Quellen und offene Ressourcen (OER)_
Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/java-springboot/addressbookapp]() und sind zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
