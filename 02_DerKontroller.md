# Der Controller

[Dieser Text ist Bestandteil eines Tutorials zur Erstellung einer einfachen AdressbuchApp mit SpringBoot/Java](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

Zunächst werden wir einen Controller für eine ReST-API-Schnittstelle programmieren, der auf einfache HTTP-Requests antwortet und JSON-Repräsentationen der Ressource zurückgibt bzw. übernimmt.

## Konfiguration und Abhängigkeiten

Um eine Controller-Klasse zu erstellen  wählen wir unter "New" / "Other"...

![](images/08_NewRESTController.png)

... im Ordner "Spring Framework" die "REST Controller Class"

![](images/09_NewRESTController.png)

![](images/10_CreateAddressController.png)

... und erhalten ein Klassenfragment, dass etwa so aussieht:

```java
package de.csbme.IFAXX.myAddressBookApp.address;
/* viele Imports */

@RestController
@RequestMapping("/url")
public class AddressController { /* */ }
```
Zunächst passen wir die mit `@RequestMapping(path = "XXX")` übergebene URL an (`path=` kann auch weggelassen werden).

```java
@RequestMapping(path = "/api/address")
```

Das Spring-Framework erkennt einzelne Komponenten über (zusammengesetzte) Annotations.

Bei `@RestController` handelt es sich um eine zusammengesetzte Annotation aus den beiden Annotationen:

- `@Controller`: Kennzeichnen eine `@Component`, die per Componentscan gefunden wird und zugleich i.d.R. per annotierten RequestHandlern festlegt, wie mit Requests verfahren wird.

- `@ResponseBody`: legt fest, dass der Rückgabewert der Methoden als Response des Webrequests gewertet werden soll. Wir wollen in diesem Fall auf einen Requests mit den angeforderten JSON-Objekten und dem jeweiligen HTTP-Status antworten.

![](plantuml/ComponentAnnotations.png)

## Die erzeugten Methoden

Darüber hinaus wurden (bei Zuhilfenahme von Netbeans) eine Reihe von Methodenrümpfen angelegt, die noch individualisiert werden müssen:

- `list()`: eine Methode, die per GET offensichtlich eine Liste aller Adressen zurückgeben soll,

- `get(), put(), post(), delete()`: vier Methoden für die HTTP-Methoden GET, PUT, POST, DELETE mit Zuordnung zu einer id (also scheinbar einer definierten Instanz von Address),

- `handleError()`: eine Methode, die zur Fehlerbehandlung dienen soll.

## Individualisierung der CRUD-Methoden

Der Zugriff auf das Model wird über das erstellte RepositoryInterface `AddressRepository` realisiert. Über die `@Autowired`-Annotation wird dieses per DependencyInjection injeziert. Den Import für die Annotation (`org.springframework.beans.factory.annotation.Autowired`) sollte Netbeans automatisch finden...

```java
    @Autowired
    private AddressRepository addressRepository;
```

Für die Methoden, die die jeweiligen HTTP-Verben behandeln, müssen wir zugehörige Methoden des Registry-Interfaces finden und diese zuweisen. Die Methoden sind hier in der Reihenfolge genannt, die sich am einfachsten testen lässt: erzeugen, lesen, ändern, löschen (_create, read, update, delete_: CRUD)

### POST (Ressourcen erzeugen: das Create von CRUD):

Per POST-Requests werden neue Ressourcen erzeugt. Der Request erfolgt auf die Klassen-URL ("/api/address").

Als Rückgabewert nutzen wir eine `ResponseEntity<>`-Instanz. Dies ist eine einfache Klasse, die sowohl den Wert (den Body/Content der Antwort), den Header und den HTTP-Status aufnimmt.

Die Klassen `ResponseEntity<>` bietet eine Reihe von Konstruktoren und statischen Methoden, um ein `ResponseEntity<>`-Objekt zu bauen, an. Die statischen Methoden (wie das von uns genutzte  `status()`) setzten das _Builder-Pattern_ um, dass zunächst ein _Builder_-Objekt erzeugt, dessen _Single Responsibility_ es ist, eine `ResponseEntity<>` zu bauen. Dieser _Builder_  (hier: `BodyBuilder`) bietet weitere Methoden an, mit denen das zu bauende Objekt konfiguriert werden kann, um dann schließlich mit einer finalen Methode (hier `body()`) das Objekt selbst zurückzugeben.

![](plantuml/ResponseEntity.png)

Im einzelnen sind dies die folgenden Schritte:
  - Erzeugung des `BodyBuilder`-Objekts und Festlegung der ersten Attribute von `ResponseEntity`. Der passende Status für neu erzeugte Ressourcen ist der Code `201`, den wir über die Konstante `HttpStatus.CREATED` übergeben können.

  ```java
  ResponseEntity.status(HttpStatus.CREATED)
  ```
  - Setzen weiterer Attribute: Es soll ein JSON-Objekt zurückgegeben werden:

  ```java
  .contentType(MediaType.APPLICATION_JSON)
  ```

  - Festlegen des Request-Bodies (das Address-Objekt) und Rückgabe des `ResponseEntity`-Objekts:

  ```java
  .body(addressRepository.save(address));
  ```

Als ganzes sieht die Methode dann so aus:

```java
@PostMapping
public ResponseEntity<Address> post(@RequestBody Address address) {
           return ResponseEntity
                   .status(HttpStatus.CREATED)
                   .contentType(MediaType.APPLICATION_JSON)
                   .body(
                           addressRepository.save(address)
                   );
}
```
Dieses Verfahren können wir auch für die anderen Methoden übernehmen.

#### Manuelles Testen
POST-Requests senden die Daten im HTTP-Body. Wir übertragen die Daten als JSON-Objekt. Wichtig ist, dass wir bei den Requests auch den ContentType und ggf. das _encoding/charset_ anpassen.

In cURL wird der Body über die `--data` oder `-d`-Option übergeben:
```bash
$ curl -D - -X 'POST' -d '{"firstName":"Hannes","lastName":"Stein"}' -H 'Content-Type: application/json' http://localhost:8085/api/address
```

Es wird das gefoderte Objekt als JSON zurück gegeben. An Hand der neu zugewiesenen `id` können wir erkennen, dass das Objekt tatsächlich gespeichert wurde.

```
HTTP/1.1 201
Content-Type: application/json
Transfer-Encoding: chunked
Date: Mon, 07 Sep 2020 03:47:40 GMT

{"id":4,"firstName":"Hannes","lastName":"Stein"}
```

Wer mit Hilfe der PowerShell testen will, muss das _Cmdlet_ `Invoke-WebRequest` nutzen. Es ist die Entsprechung zu cURL, nutzt aber eine andere Syntax. Die Parameter müssen wie folgt übergeben werden:

```powershell
> Invoke-WebRequest http://localhost:8085/api/address -Method 'POST' -ContentType 'applicatiOn/json; charset=utf-8'  -Body '{"firstName":"Martin","lastName":"Mustermann"}'
```
Das Ergebnis ist auch hier wie erwartet ein Statuscode 201 (HTTP Created) und eine zugewiesene `id`.

```
StatusCode        : 201
StatusDescription :
Content           : {"id":1,"firstName":"Martin","lastName":"Mustermann"}
RawContent        : HTTP/1.1 201
                    Transfer-Encoding: chunked
                    Keep-Alive: timeout=60
                    Connection: keep-alive
                    Content-Type: application/json
                    Date: Sat, 29 Aug 2020 17:04:40 GMT

                    {"id":1,"firstName":"Martin","lastName...
Forms             : {}
Headers           : {[Transfer-Encoding, chunked], [Keep-Alive, timeout=60], [Connection, keep-alive], [Content-Type,
                    application/json]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 53
```

### GET (Ressourcen lesen: das Read von CRUD)

#### GET all

Die erste der beiden GET-Methoden ist zuständig für alle GET-Requests auf die Klassen-URL ("/api/address"), daher werden unter `@GetMapping` keine weiteren Einschränkungen vorgenommen. Es soll eine Liste aller Adressen ausgegeben werden. Die Methode `findAll()` des `CrudInterface` liefert uns ein `Iterable` zurück, dass wir nutzen können.

Auch hier wollen wir eine `ResponseEntity` zurückgeben, die wir nach dem Muster der obigen Methode erstellen. Als Status geben wir `HttpStatus.OK` zurück, im Body geben wir die Rückgabe der Repository-Methode  `findAll()` an. Dementsprechend sollten wir es anpassen:

```java
@GetMapping()
public ResponseEntity<Iterable<Address>> list() {
    return ResponseEntity
            .status(HttpStatus.OK)
            .contentType(MediaType.APPLICATION_JSON)
            .body(
                    addressRepository.findAll()
            );
}
```
Bevor wir das testen können, müssen wir natürlich über POST eine Ressource erzeugen (siehe oben). Getestet werden kann dieser GET-Request mit `cURL`:

```bash
$ curl -D - -s http://localhost:8085/api/address
```
```
HTTP/1.1 200
Content-Type: application/json
Transfer-Encoding: chunked
Date: Mon, 07 Sep 2020 04:07:44 GMT

[{"id":1,"firstName":"Hannes","lastName":"Stein"},{"id":2,"firstName":"Peter","lastName":"Lustig"}]
```

Die Option `-D -` gibt auch den ResponseHeader zurück, mit -s werden einige zusätzliche Angaben ausgeblendet. Da `GET` der Defaultwert ist, kann die Option `-X GET` weggelassen werden.

In der PowerShell entsprechend:

```powershell
> Invoke-WebRequest http://localhost:8085/api/address -Method 'GET'
```
```
StatusCode        : 200
StatusDescription :
Content           : [{"id":1,"firstName":"Hannes","lastName":"Stein"}]
RawContent        : HTTP/1.1 200
                    Transfer-Encoding: chunked
                    Keep-Alive: timeout=60
                    Connection: keep-alive
                    Content-Type: application/json
                    Date: Mon, 07 Sep 2020 04:02:10 GMT

                    [{"id":1,"firstName":"Hannes","lastNam...
Forms             : {}
Headers           : {[Transfer-Encoding, chunked], [Keep-Alive, timeout=60], [Connection, keep-alive], [Content-Type, application/json]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 50
```

Auch hier ist `GET` der Defaultwert und die Option `-Method 'GET'` kann weggelassen werden.

#### GET one

Wenn mit dem GET-Request zusätzlich noch eine ID übergeben wird, soll nur eine Adresse zurückgegeben werden. Es gibt zwei Methoden, wie wir hierzu nutzen können - abhängig davon, welches Repository-Interface wir für `AddressRepository` gewählt haben:
`findById()` von `CrudRepository` gibt uns ein generisches `Optional` zurück, `getOne()` des `JpaRepository` den generischen Typen (in unserem Fall also `Address`). Die Methode muss zunächst wieder annotiert und angepasst werden:

- `@GetMapping("/{id}")` identifiziert den Wert, der im aufgerufenen Pfad an Stelle des Platzhalters (`{id}`) im `@GetMapping`-Parameters steht als _Eingabe_, die wir später mit der Referenz `id` nutzen wollen.

 - Die Parameter-Annotation `@PathVariable long id` bindet diesen Wert direkt an unseren Methodenparameter `id`.

- Die `ResponseEntity` setzt sich aus dem Status `HttpStatus.OK`, dem gewohnten ContenType JSON und dem Rückgabewert unserer Repository-Methode `findById(id)` zusammen:

```java
@GetMapping("/{id}")
 public ResponseEntity<Optional<Address>> get(@PathVariable long id) {
     return ResponseEntity
             .status(HttpStatus.OK)
             .contentType(MediaType.APPLICATION_JSON)
             .body(
                     addressRepository.findById(id)
             );
 }
```

Getestet werden kann dieser GET-Request mit `curl` (Natürlich erst, nachdem - wie oben zu sehen - Datensätze angelegt wurden... vorher wird `null` zurückgegeben.)

```bash
$ curl -D - http://localhost:8085/api/address/1
```

```
HTTP/1.1 200
Content-Type: application/json
Transfer-Encoding: chunked
Date: Mon, 07 Sep 2020 04:04:35 GMT

{"id":1,"firstName":"Hannes","lastName":"Stein"}
```

In der PowerShell entsprechend:
```powershell
> Invoke-WebRequest http://localhost:8085/api/address/1 -Method 'GET'
```

```
StatusCode        : 200
StatusDescription :
Content           : {"id":1,"firstName":"Hannes","lastName":"Stein"}
RawContent        : HTTP/1.1 200
                    Transfer-Encoding: chunked
                    Keep-Alive: timeout=60
                    Connection: keep-alive
                    Content-Type: application/json
                    Date: Mon, 07 Sep 2020 04:10:46 GMT

                    {"id":1,"firstName":"Hannes","lastName...
Forms             : {}
Headers           : {[Transfer-Encoding, chunked], [Keep-Alive, timeout=60], [Connection, keep-alive], [Content-Type, application/json]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 48
```


### PUT:

Im Gegensatz zu GET, das alle Informationen aus dem HTTP-Header übergibt, erhält PUT die Infos aus dem HTTP-Body. Das muss über Annotationen festgelegt werden:

- `@RequestBody Address address` verknüpft den HTTP-Nachrichtenbody mit unserem Methodenparameter

- `@PathVariable long id`: siehe Erlärung aus Get-Request oben: der Wert von `ìd` wird aus dem aufgerufenen Pfad übernommen.

Diese Methode wird etwas aufwändiger: theoretisch kann die ID einer zu ändernden Adresse über die URL und über den gesendeten Content übergeben werden. Es wird geprüft:

- Wurde keine `id` mit dem JSON-Objekt im Body übergeben wird die `id` der URL übernommen, wenn möglich.
- Sind `id` der URL und des JSON-Objekts identisch, existiert aber dafür kein Eintrag in der DB wird ein Fehler geworfen.
- Wurde keine `id` der URL übergeben, jedoch eine per JSON, so wird diese verwendet (derzeit ungeprüft).

- Falls die übergebenen `id` per JSON und URL abweichen oder die zu ändernde Ressource nicht existiert wird ebenfalls ein Fehler geworfen.

Die `ResponseEntity` baut sich wiederum aus Status, Contenttype und dem Rückgabewert der Repository-Methode `save(address)` auf.

```java
@PutMapping("/{id}")
public ResponseEntity<Address> put(@PathVariable long id, @RequestBody Address address) {
    if ((address.getId() == null) || (address.getId() == 0)) {
        if (id != 0) {
            address.setId(id);
        } else {
            throw new RuntimeException("Es wurde kein zugehöriges Address-Objekt zum Updaten gefunden!");
        }
    } else if (address.getId() == id) {
        if (!(addressRepository.existsById(id))) {
            throw new RuntimeException("Es wurde kein zugehöriges Address-Objekt zum Updaten gefunden!");
        }
    } else if ((id == 0) && (((address.getId() != 0)) || (address.getId() != null))) {
        id = address.getId();
    } else if ((address.getId() != id) || !(addressRepository.existsById(id))) {
        throw new RuntimeException("Es wurde kein zugehöriges Address-Objekt zum Updaten gefunden!");
    }
    return ResponseEntity
            .status(HttpStatus.OK)
            .contentType(MediaType.APPLICATION_JSON)
            .body(addressRepository.save(address));
}
```

Der manueller Test  kann natürlich wieder nur erfolgen, wenn vorher bereits Ressourcen per `POST` erstellt wurden (siehe oben). Hier angegeben ist jeweils nur der Test für den _happy path_ - probieren Sie ruhig aus, ob auch die oben genannten Sonderfälle wie erwartet funktionieren.

Mit `curl`:

```bash
$ curl -D - -d '{"firstName":"Andreas","lastName":"Geändert", "id":"1"}' -H 'Content-Type: application/json' -X PUT http://localhost:8085/api/address/1
```
```
HTTP/1.1 200
Content-Type: application/json
Transfer-Encoding: chunked
Date: Mon, 07 Sep 2020 04:50:29 GMT

{"id":1,"firstName":"Andreas","lastName":"Geändert"}
```

Mit der PowerShell kann so getestet werden:

```PowerShell
Invoke-RestMethod http://localhost:8085/api/address/1 -Method 'PUT' -ContentType 'application/json; charset=utf-8' -Body '{"firstName":"Michael", "lastName":"Nochmalanders", "id":"1"}'
```

```
id firstName lastName
-- --------- --------
 1 Michael   Nochmalanders
```

### DELETE:

Die `DELETE`-Requests sind dagegen wieder einfach: Wenn eine Ressource mit dieser ID existiert, muss sie gelöscht werden - andernfalls wird eine Exception geworfen.

Die `ResponseEntity` enthält hier keinen Content und gibt lediglich zurück, dass sie ausgeführt wurde (Statuscode 203) wird per `noContent()` generiert

```java
@DeleteMapping("/{id}")
 public ResponseEntity<Address> delete(@PathVariable long id) {
     if (!(addressRepository.existsById(id))){
         throw new RuntimeException("Es wurde kein zugehöriges Address-Objekt zum Löschen gefunden!");
     }
     addressRepository.deleteById(id);
     return ResponseEntity
             .noContent()
             .build();
 }
```

Für das manuelle Testen müssen wir natürlich zunächst per `POST` eine Ressource erzeugen. Entsprechend müssen die Befehle von oben zuvor ausgeführt werden, dann:

```bash
$ curl -D - -s -X DELETE http://localhost:8085/api/address/2
```

Wenigstens der HTTP-Status kann hier kontrolliert werden:

```
HTTP/1.1 204
Date: Mon, 07 Sep 2020 04:56:00 GMT
```

Die Powershell erzeugt hierbei gar keine Ausgabe
```PowerShell
Invoke-RestMethod http://localhost:8085/api/address/1 -Method 'DELETE'
```

Ob die Ressourcen wirklich gelöscht wurden kann mit einem anschließenden GET-Request geprüft werden.


### Fazit

Der ReST-Controller wurde erfolgreich erstellt und die API kann offensichtlich benutzt werden. Trotzdem sind die manuellen Tests insbesondere bei weiteren Anpassungen natürlich mühsam, daher werden im folgenden Schritt per jUnit automatische Tests des Controllers erstellt:

[Automatisches Testen des Controllers](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/02a_AutomatischesTestenDesControllers.html)

## Links

- [Hinweise zur Umsetzung mit IntelliJ](https://www.jetbrains.com/help/idea/spring-support-tutorial.html)

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/java-springboot/addressbookapp]() und sind zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
