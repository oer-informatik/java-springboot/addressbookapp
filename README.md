# AddressBookApp mit Java und dem SpringBoot Framework

Infodokumente, erstellt mit den Konfiguraiton der [TIBHannover](https://gitlab.com/TIBHannover/oer/course-metadata-test/) für die CI-Pipeline von Gitlab.

* [Initialisierung des Projekts](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html) [(als PDF)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.pdf)
* [Erstellung des Models](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/01_DasModell.html) [(als PDF)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/01_DasModell.pdf)
* [Erstellung des Controllers](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/02_DerKontroller.html) [(als PDF)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/02_DerKontroller.pdf)


# Nachnutzung


Dieses Dokument wurde mit Hilfe dieser Vorlage (https://gitlab.com/TIBHannover/oer/course-metadata-test/) erstellt.

Projekt als Vorlage für eigene Kurse verwenden.

* dieses Repository auf GitLab clonen
* metadata.yml anpassen
    * manuell
    * mit [Metadaten-Generator](https://tibhannover.gitlab.io/oer/course-metadata-gitlab-form/metadata-generator.html) // UNDER CONSTRUCTION!
* course.md anpassen
* Links in der README.md anpassen

Beim ersten Durchlauf kann es bis zu ca. 15min dauern, bis die Dateien generiert sind. Weitere Änderungen stehen i.d.R. nach <1min bereit.

