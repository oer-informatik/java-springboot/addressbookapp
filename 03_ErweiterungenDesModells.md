# Erweiterung der Annotationen für Beziehungen der Entitätstypen

Das Modell wurde bewusst sehr rudimentär gehalten, um zunächst einen Durchstich durch alle Schichten einer SpringBoot-App zu ermöglichen. In der Praxis stehen natürlich einzelne Modell miteinander in Beziehungen. Ohne das jetzt bereits realisieren zu wollen, sollen hier zumindest Beispiele genannt werden, wie Beziehungen zwischen Tabellen (Modell) realisiert werden können. Im nächsten Abschnitt (link unten) wird dann die View realisiert.  

#### `@OneToOne`
Als Beispiel: jede Adresse wird mit genau einem Geokoordinatensatz verknüpft - und umgekehrt. Diese sind im Modell GeoCoord.java abgebildet. Das Attribut im Address-Modell müsste wie folgt annotiert werden:
```
@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL, targetEntity=GeoCoord.class)
@JoinColumn(name="GeoCoord", nullable=false)
@OnDelete(action=OnDeleteAction.CASCADE)
private GeoCoord geoCoord;
```

#### `@ManyToMany`
Beispielhaft: Eine Adresse kann mehreren Gruppen angehören, und jede Gruppe besteht aus mehreren Adressen. Um eine solche _n-zu-m_ -Beziehung mit JPA abzubilden wird ein Collection-Attribut benötigt (hier als HashSet, der parametriete Typ ist in diesem Beispiel direkt die Modelklasse, idealerweise wird hier jedoch ein Interface  verwendet):
```
private Set<GruppeModel> gruppen = new HashSet<>();
```
Dieses Attribut muss wie folgt annotiert werden:
```
@ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.ALL},targetEntity= GruppeModel.class)
@JoinTable(name = "Adresse_Gruppe",
        joinColumns = { @JoinColumn(name = "ID_Adresse") },
        inverseJoinColumns = { @JoinColumn(name = "ID_Gruppe") })
private Set<GruppeModel> gruppen = new HashSet<>();
```
Individualisiert werden müssen hier die Parameter:
 - `@ManyToMany targetEntity`: Bezeichnung der verknüpften Modellklasse
 - `@JoinTable name`: Bezeichnung der Verknüpfungstabelle
 - `@JoinTable joinColumns @JoinColumn name`: Fremdschlüssel ID in diesem Modell
 - `@JoinTable   inverseJoinColumns @JoinColumn name`: Fremdschlüssel ID in dem verknüpften Modell

Im Fall einer _n-zu-m_ -Beziehung muss bei der verknüpften Tabelle entsprechend mit vertauschten Rollen die analoge Annotation erfolgen.
```
@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,CascadeType.MERGE},
            mappedBy = "address")
private Set<Address> addresses = new HashSet<>();
```

[Die weiteren Schritte finden sich in diesem Tutorial (link)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/04_TemplateEngineThymeleaf.html)

## Links und weitere Informationen

## _Quellen und offene Ressourcen (OER)_
Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/java-springboot/addressbookapp]() und sind zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
