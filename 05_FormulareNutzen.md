# Formulare mit Thymeleaf und der zugehörige Controller

[Dieser Text ist Bestandteil eines Tutorials zur Erstellung einer einfachen AdressbuchApp mit SpringBoot/Java](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

## Ziel dieses Abschnitts:

Es soll in diesem Abschnitt:

- eine Textseite zur Anzeige einer Adresse und eine Listenausgabe zur Anzeige aller Adressen erstellt werden (jeweils Template & Controller)

- ein HTML-Formular-Template zum erstellen neuer und aktualisieren vorhandener Adressen sowie die zugehörigen Controllermethoden zur Entgegennahme und Speicherung der Daten des (bereits bestehende) `Address`-_Models_.

Voraussetzung ist, dass die Abhängigkeiten zu Thymeleaf wie im letzten Abschnitt genannt in der `pom.xml` angegeben wurden

## Identifikation der nötigen Zustände und Zustandsübergänge

Über ein Zustandsdiagramm, in dem wir die einzelnen Zustände der GUI verzeichnen können wir einen Weg durch die App planen. Hilfreich ist dies insbesondere, um einen Überblick über die erforlderlichen _Controller_-Methoden, Routen und Views zu erhalten.



* Startpunkt ist die noch leere App. Es soll eine leere Adress-Liste angezeigt werden. Der Einfachheit halber soll hier die selbe Ansicht gewählt werden, die wir später für die gefüllte Liste nutzen (View 1: `adresses.html`). Diese Liste soll beim Aufruf (`GET`) der Adresse: `http://127.0.0.1:8085/address` ausgegeben werden.

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp-step1.png)

* Einziger Event, der uns aus dieser Ansicht bringt soll ein Klick auf einen Button "Neue Adresse ergänzen" sein. Später wird die selbe _View_ weitere Events bereithalten, um Listeneinträge zu bearbeiten. Diese fehlen aber in diesem Zustand zunächst noch. Der Event soll einen `GET`-Request auf die Route `http://127.0.0.1:8085/address/edit/0` auslösen. Wir nutzen die selbe Route, die wir später für das Editieren vorhandener Adressen nutzen wollen, übergeben aber für diesen neuen Eintrag die `id=0`.

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp-step2.png)

* Der folgende Zustand ist die Eingabemaske (View 2: `addressEdit.html`) für neue Einträge. Auch diese View soll mehrfach verwendet werden: später soll sie auch zur Änderung vorhandener Einträge verwendet werden.

* Über den Event _"Speichern"-Button drücken_ wird die Geschäftslogik zum Speichern aufgerufen.  Es wird geprüft (Pseudozustand _choice), ob es sich um einen neuen (_Guard_: `[id ==0 || id is NULL]`) oder einen bearbeiteten (`[else]`) Eintrag handelt. Nachdem die internen Aktivitäten (hier: die _entry-Aktivität_ des Zustands: `create()`) abgeschlossen sind wird dieser Zustand direkt verlassen (kein Event zum Zustandstransition angegeben).

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp-step3.png)

* Nach dem Speichern wird automatisch zur Ansicht der (dann) gefüllten Liste (View 1: `adresses.html`) weitergeleitet. Im Zustandsdiagramm wird das durch den immer gültigen _Guard_ `[true]` und den bei Transition aufgerufenen _Effekt_ `\Weiterleitung an list()` notiert. _Guard_ und _Effekt_ müssten im UML-Zustandsdiagramm nicht unbedingt angegeben werden - sie sind hier nur zur Verdeutlichung des Automatismus genannt.

* In der Liste können drei Events ausgelöst werden:

  * "Details bearbeiten" und "Neue Adresse ergänzen" führen beide wieder zur Eingabemaske (View 2: `addressEdit.html`)

  * "Details anzeigen" führt zu einer weitern Maske, die ohne Bearbeitungs-Möglichkeit alle Details anzeigt. (View 2: `addressDetails.html`)

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp-step4.png)


* Aus der Eingabenmaske muss es noch einen Weg für das Löschen und Editieren eine Adresse geben. Editieren hatten wir oben bereits vorgesehen, Löschen wurde noch ergänzt. Die Zustände und Transitionen können analog zum Zustand "Neuer Eintrag gespeichert" geplant werden.

Wir kommen somit in einem einfachen Oberflächen-Design auf  drei _Views_, sieben Zustände und sechs verschiedene Routen:

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp.png)


## Eine _Controller_-Klasse für die Thymeleaf-Views

Nachdem auf der vorigen Seite ein wenig mit der Thymeleaf-Engine herumexperimentiert wurde, wollen wir sie nun nutzen, um ein Formular für das Adressbuch zu erstellen.

Dazu benötigen wir für die drei oben identifizierten _Views_ jeweils ein HTML-Template, dass wieder im Ordner `src\main\resources\templates` gespeichert werden muss. Die _Views_ müssen jeweils zugehörige Controller haben, die die Templates laden und befüllen.

Wir legen diesen _Controller_ in das gleiche Paket. Im Kontextmenü können wir unter `New/Controller Class` (wichtig: nicht Rest-Controller!!) eine Vorlage erstellen:

![Einen neuen Controller erstellen unter New-ControllerClass](images/NewController.png)

Der Controller, den die Springboot-Unterstützung von Netbeans anlegt sieht zunächst so aus:

```java
package de.csbme.IFAXX.myAddressBookApp.address;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AddressWebController {

    @RequestMapping("/url")
    public String page(Model model) {
        model.addAttribute("attribute", "value");
        return "view.name";
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", req.getRequestURL());
        // add other objects to the model here
        modelAndView.setViewName("error-view-name");
        return modelAndView;
    }
}
```

## Anpassen des Controllers

Die erzeugte _Controller_-Methode zeigt exemplarisch, was in unseren eigenen Methoden umgesetzt werden muss:

- Festlegung des Routings per `@RequestMapping(...)`-Annotation

- Anpassen /Auslesen des _Model_

- Zusammenstellen der View - wobei wir die Variante wählen, dass als Rückgabewert nicht der Viewname als String genutzt wird (wie in diesem Beispiel), sondern ein Objekt der Klasse `ModelAndView`. Hierdurch ist die direkte Beeinflussung des _Model_s leichter.

Die Exception-Handler Methode können wir zunächst behalten, die andere Methode kann getrost (nach einem Blick auf die dahinterstehende Logik) gelöscht werden.

### Einbinden des Repositories

Analog zu unserem ReST-Controller müssen wir auch hier das Repository per `@Autowired` einbinden, um auf das _Model_ zugreifen zu können:

```java
@Autowired
private AddressRepository addressRepository;
```

## Adressen auflisten

Der erste Schritt sollte die Auflistung bestehender Ressourcen sein:

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp-step1.png)

### Die Controller-Methoden

In der _Controller_-Methode zur Auflistung von Adressen müssen die folgenden Schritte umgesetzt werden:

- das richtige Routing gesetzt werden: `@GetMapping("/address")`

- analog zum ReST-Controller das Model geladen werden:
`Iterable <Address> addresses = addressRepository.findAll();`

- das `Model` und der Name der View an das als Parameter übergebene Framework-Objekt `modelAndView` übergeben werden:
  ```java
  modelAndView.addObject("addresses", addresses);
  modelAndView.setViewName("addresses");
  ```

Im Ganzen ist die Methode recht überschaubar:

```java
@GetMapping("/address")
public ModelAndView list(ModelAndView modelAndView) {
    Iterable <Address> addresses = addressRepository.findAll();
    modelAndView.addObject("addresses", addresses);
    modelAndView.setViewName("addresses");
    return modelAndView;
}
```

Ich habe darüber hinaus temporär eine Route erstellt, die die Datenbank mit Beispieleinträgen füllt - das macht das manuelle Testen leichter - muss natürlich in der Produktion gelöscht werden...

```java
@GetMapping("/testaddress")
public ModelAndView testList(ModelAndView modelAndView) {
    addressRepository.save(new Address("Anton", "Aal"));
    addressRepository.save(new Address("Bertram", "Busch"));
    addressRepository.save(new Address("Martin", "Mustermann"));
    return list(modelAndView);
}
```

Dadurch kann durch Aufrufen der URL `http://127.0.0.1:8085/testaddress` nach Einbindung der _View_ (nächste Abschnitt) direkt geprüft werden, ob die Methoden soweit wie erwartet funktionieren.

### Die zugehörige View

Im _Controller_ wurde in der `list`-Methode oben angegeben, dass die zugehörige _View_ `addresses`heißt:

```java
modelAndView.setViewName("addresses");
```

Wir müssen also eine Datei `addresses.html` mit dem Template für die Adress-Übersicht unter `/src/main/resources/templates` anlegen, denn dort sucht die _Thymeleaf_-Engine danach. Das Template könnte etwa so aussehen:

```html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org" th:lang="${#locale.language}">
    <head>
        <title>Address</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="" th:href="@{/css/stylesheet.css}"/>
    </head>
    <body>
        <table>
            <tr th:each="address: ${addresses}">
                <td th:text="${address.id}"/>
                <td th:text="${address.firstName}"/>
                <td th:text="${address.lastName}"/>
                <td>
                  <a href="/address" class="buttonlink" th:href ="@{/address/{addressId}(addressId=${address.id})}">Details anzeigen</a> /
                  <a href="/address" class="buttonlink" th:href ="@{/address/edit/{addressId}(addressId=${address.id})}">Details bearbeiten</a>
                </td>
            </tr>
        </table>
        <a href="/address" th:href="@{/address/edit/0}">Neue Adresse ergänzen</a>
    </body>
</html>
```

Allgemein gesprochen unterstützt Thymeleaf eine Reihe von Platzhaltern:

- Wir können Werte an beliebiger Stelle durch Variableninhalte ersetzten, der Platzhalter wird mit `${}` festgelegt: `${address.id}"`

- Links zu anderen Dokumenten, wie in der CSS-Verknüpfung des `<link />`-tags mit `@{}`: `th:href="@{/css/stylesheet.css}"`

- Auswahl-Platzhalter (`*{}`), die in Kurzform auf ein per `th:object="${xyz}"` referenziertes Objekt zeigen, sind in diesem Beispiel nicht genutzt.

- Mit `#{}`  gekennzeichnete Mitteilungen  könnten auch noch im Template stehen.

- Darüber hinaus gibt es noch den Platzhalter für zusammengesetzte Fragmente, der wird hier nicht genutzt, würde aber mit `~{}` notiert.


Neu an diesem Template ist die _Thymeleaf_- Funktion für HTML-Elemente, die über  Objektinhalte iterieren:

```html
<tr th:each="address: ${addresses}">
```

Die so notierte Tabellenzeile `<tr>...</tr>` wird für jedes Element (`address:`) des übergebenen _Model_-Arrays (`${addresses}`) wiederholt.

Weiterhin werden im Template Links generiert, die jeweils um die ID (`{addressId}`) der einzelnen Zeilen erweitert werden. Die Formel (`@{/address/{addressId}(addressId=${address.id})}`) zur Generierung wird als link übergeben:

```html
<a href="/address" class="buttonlink" th:href ="@{/address/{addressId}(addressId=${address.id})}">Details</a>
```

Mit dem CSS-Klassenselektor `class="buttonlink" ` haben wir später die Möglichkeit, diese Links wie Buttons aussehen zu lassen.

## Einzelne Adressen anzeigen

Bei der Planung hatten wir den Zustand "Details angezeigt" versehen mit der Route `/address/{id}` und der Methode `showDetails()`:

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp-step4.png)

### Die _Controller_-Methode für Adress-Details einfügen

Eine einzelne Adresse soll über die URL `/address/{id}` erreicht werden können:
```java
@GetMapping("/address/{id}")
```
 und ein eigenes Template `addressDetail.html` verwenden:

 ```java
modelAndView.setViewName("addressDetail");
 ```
Die übrige Logik kann vom entsprechenden ReST-Controller übernommen werden.

Im Ganzen:

```java
@GetMapping("/address/{id}")
public ModelAndView showDetails(@PathVariable long id, ModelAndView modelAndView) {
    Optional<Address> optAddress = addressRepository.findById(id);
    if (optAddress.isPresent()) {
        modelAndView.addObject("address", optAddress.get());
    }else{
        throw new RuntimeException("kein Address-Objekt mit der ID " + id);
    }
    modelAndView.setViewName("addressDetail");
    return modelAndView;
}
```

### Die zugehörige View für Adress-Details

Um einzelne Adressdetails auszugeben nutzen wir ein eigenes Template
Hierzu speichern wir eine Datei `addressDetail.html` unter `/src/main/resources/templates` mit folgendem Inhalt:

```html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org" th:lang="${#locale.language}">
    <head>
        <title>Address</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="" th:href="@{/css/stylesheet.css}"/>
    </head>
    <body>
        <table>
            <tr >
                <td>ID:</td>
                <td th:text="${address.id}"/>
            </tr>
            <tr>
                <td>Vorname:</td>
                <td th:text="${address.firstName}"/>
            </tr>
            <tr>
                <td>Nachname:</td>
                <td th:text="${address.lastName}"/>
            </tr>
        </table>
        <a href="/address" th:href ="@{/address/edit/{addressId}(addressId=${address.id})}">Diese Adresse bearbeiten</a>
    </body>
</html>
```

## Adressen editieren

Um Adressen editieren zu können werden zwei Controllermethoden benötigt:
Eine Methode öffnet das zugehörige Formular (`editDetails()`), eine zweite Methode übernimmt die Daten aus dem Formular und zeigt das Ergebnis an (`update()`).

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp-step3.png)

### Controller-Methode zur Darstellung des Formulars:

Diese Methode ähnelt stark der Methode zur Detail-Darstellung. Lediglich die Route und die zugehörige View unterscheiden sich:

```java
@GetMapping("/address/edit/{id}")
public ModelAndView editDetails(@PathVariable long id, ModelAndView modelAndView) {
    Optional<Address> optAddress = addressRepository.findById(id);
    if (optAddress.isPresent()) {
        modelAndView.addObject("address", optAddress.get());
    }else{
        modelAndView.addObject("address", new Address());
    }
    modelAndView.setViewName("addressEdit");
    return modelAndView;
}
```

### Controller-Methode zur Übernahme der Werte

Wie bereits beim ReST-Controller ist auch hier in der Update-Methode einiges an Logik erforderlich, um das korrekte Model zu wählen. Dieser Code kann 1:1 übernommen werden.

Im Anschluss dieser Logik wird kein eigenes ModelAndView ausgewählt, sondern die Liste mit allen neuen Elementen angezeigt:

```java
   return list(modelAndView);
```

Im Ganzen:

```java
@PostMapping("/address/{id}")
public ModelAndView update(@PathVariable long id, @ModelAttribute Address address, ModelAndView modelAndView) {
    if ((address.getId() == null)||(address.getId()==0)) {
        if (id != 0){
            address.setId(id);
        }else{
            throw new RuntimeException("Es wurde kein zugehöriges Address-Objekt zum Updaten gefunden!");
        }
    }else if (address.getId() == id){
        if (!(addressRepository.existsById(id))){
            throw new RuntimeException("Es wurde kein zugehöriges Address-Objekt zum Updaten gefunden!");
        }
    } else if ((id == 0) && (((address.getId() != 0))||(address.getId() != null))) {
        id = address.getId();
    }else if ((address.getId()!= id) || !(addressRepository.existsById(id))){
          throw new RuntimeException("Es wurde kein zugehöriges Address-Objekt zum Updaten gefunden!");
    }
   addressRepository.save(address);
   return list(modelAndView);
}
```

### Die zugehörige View

Hierzu speichern wir eine Datei `addressEdit.html` unter `/src/main/resources/templates` mit folgendem Inhalt:

```html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org" th:lang="${#locale.language}">
    <head>
        <title>Address</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="" th:href="@{/css/stylesheet.css}"/>
    </head>
    <body>
        <form th:object="${address}" method="POST">
            <table>
                <tr>
                    <td>
                        <label for="firstName" th:text="#{firstName}">Vorname</label>
                    </td>
                    <td>
                        <input type="text" th:field="*{firstName}" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="lastName" th:text="#{lastName}">Nachname</label>
                    </td>
                    <td>
                        <input type="text" th:field="*{lastName}" />
                    </td>
                </tr>
            </table>
            <input type="submit" formaction="" th:formaction="@{/address/{addressId}(addressId=${address.id})}" value="Änderungen Speichern" />
            <input type="submit" formaction="" th:formaction="@{/address/delete/{addressId}(addressId=${address.id})}" value="Adresse löschen" />
            <input type="reset" />
            <a href="" th:href="@{/address/}">Zurück zur Liste</a>
        </form>
    </body>
</html>
```
Wichtige Bereiche:

1. Im `<form>`-Tag ist festgelegt, welcher Controller die Anfrage bearbeitet:
```html
<form action=""
      th:action="@{/saveAddress/}"
      th:object="${address}"
      method="POST">
```

- `th:action="@{/saveAddress/}"` gibt den Controller an, der auf die Anfrage reagieren soll

- `th:object="${address}"`: Name des _Model_-Objekts. Wird das hier als `object` angegeben, kann auf die einzelnen Attribute des _Model_ später statt über `${address.firstName}` direkt über die Kurzform `*{firstName}` referenziert werden

2. Für jedes Formularelement gibt ein `<label>` an, wofür das folgende Eingabefeld verwendet wird:

```html
<label for="firstName" th:text="#{firstName}">Vorname</label>
```
  - `for="firstName"`
  - `th:text="#{firstName}"`

3. Das eigentliche `<input>`-Feld gibt die zu speichernden Werte an:

```html
<input type="text" th:field="*{firstName}" />
```
  - `type="text"`
  - `th:field="*{firstName}"`

4. Das Absenden des Formulars kann über einen _Speichern_-Knopf erfolgen

```html
<button type="submit">Speichern</button>
```
- Auswahl-Platzhalter (`*{}`) referenzieren in Kurzform auf ein per `th:object="${xyz}"` referenziertes Objekt. Im `<input />-Tag` beispielsweise mit der Kurzform `th:field="*{firstName}"` statt `th:field="${address.firstName}"`

- Mitteilungen wie im `<label /> Tag` werden mit `#{}` notiert: `th:text="#{firstName}"`

- Links zu anderen Dokumenten, wie in der CSS_Verknüpfung des `<link />`-tags mit `@{}`: `th:href="@{/css/stylesheet.css}"`

- Darüber hinaus gibt es noch den Platzhalter für zusammengesetzte Fragmente, der wird hier nicht genutzt, würde aber mit `~{}` notiert.

## Controller zum Erzeugen einer neuen Adresse

Der Controller zum Erzeugen einer neuen Adresse soll auf einen `POST`-Request auf `/address` oder `/address/0` reagieren. Es wird keine eigene View benötigt, sondern nach erfolgter Speicherung an `list()` weitergeleitet.

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp-step3.png)

Daher kann der Controller relativ einfach umgesetzt werden:

```java
@PostMapping("/address")
public ModelAndView create(@ModelAttribute Address address, ModelAndView modelAndView) {
    addressRepository.save(address);
    return list(modelAndView);
}
```


## Controller zum Löschen einer Adresse

Der Controller zum Löschen einer Adresse soll auf den `POST`-Request `/address/delete/{id}` reagieren und benötigt nach Planung keine eigene _View_: er leitet nach getaner Arbeit die Anfrage weiter an die Methode `list()`:

![UML-Zustandsdiagramm für die GUI](plantuml/zustandsdiagramm-webapp-step4.png)

Die Logik der Methode kann weitgehend aus dem ReST-Controller übernommer werden - lediglich der Rückgabewert (in diesem Fall der Aufruf von `list()`) muss angepasst werden:

```java
@PostMapping("/address/delete/{id}")
public ModelAndView delete(@PathVariable(required = false) long id, @ModelAttribute Address address, ModelAndView modelAndView) {
    if (!(addressRepository.existsById(id))){
        throw new RuntimeException("Es wurde kein zugehöriges Address-Objekt zum Löschen gefunden!");
    }
    addressRepository.deleteById(id);
    return list(modelAndView);
}
```

## Fehlende Pfade und Routen

Wenn die _Controller_ und _Views_ soweit fertig gestellt sind kann die Oberfläche zunächst händisch getestet werden. Dabei fallen einige bislang fehlende Pfade auf, die den Bedienkomfort einschränken und noch ergänzt werden müssen. Da hierfür jedoch keine grundlegend neuen Techniken erforderlich sind, wird dies an dieser Stelle nicht gesondert beschrieben.

[Die weiteren Schritte finden sich in diesem Tutorial (link)](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/06_TestWebController.html)

## Links und weitere Informationen

- [Eine gute deutschsprachige Einführung zu Thymeleaf und Spring bietet codeflow.site](https://www.codeflow.site/de/article/thymeleaf-in-spring-mvc)

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/java-springboot/addressbookapp](https://gitlab.com/oer-informatik/java-springboot/addressbookapp).

Sie sind bei Namensnennung (Hannes Stein) zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
