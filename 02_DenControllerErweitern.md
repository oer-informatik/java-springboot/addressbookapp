# Den Controller erweitern

[Dieser Text ist Bestandteil eines Tutorials zur Erstellung einer einfachen AdressbuchApp mit SpringBoot/Java](https://oer-informatik.gitlab.io/java-springboot/addressbookapp/00_InitialisierungDesProjekts.html)

### Genauere Einflussnahme auf den Request mit `@RequestMapping`

Die genutzten Annotationen `@GetMapping`, `@PutMapping`, `@PostMapping`, `@DeleteMapping` sind _Composed Annotations_ und verweisen beispielsweise für Get auf `@RequestMapping(method = RequestMethod.GET)`.
Das Zugrunde liegende `@RequestMapping` bietet noch eine Reihe von weiteren Konfigurationsmöglichkeiten:

#### Einschränkung auf im Header des Requests übermittelte Informationen

```java
  @RequestMapping(
  value = "/dummy/{id}",
  headers = "name=hannes",
  method = RequestMethod.GET
)
public String getDummy(@PathVariable long id) {
    return "Nur Für Hannes: Du bist Nummer "+id;
}
```

Auch mehrere Header-Werte sind mögich, wenn die einzelnen Werte in geschweifte Klammern geschrieben werden:
`headers = {"name=hannes", "gruppe=team1"}`

Getestet werden kann das z.B. mit `curl`:

```bash
curl -H "name:hannes" http://localhost:8080/api/address/dummy/6
```

Hinweis: cURL übermitteltelt die im Header enthaltenen Key-Value-Paare mit Doppelpunkt, SpringBoot erwartet, dass sie mit Gleichheitszeichen getrennt werden.


## Ergänzungen / Vertiefungen

### Serviceklassen für zusätzliche Aufgaben zwischen Controller und Repository

Sofern vor der Persistenz eines Objekts noch einiges an _business logic_ erforderlich ist (aufwändigere Validierungen, Umwandlungen usw.), die nicht in den Aufgabenbereich des Controllers fallen, kann es sinnvoll sein, eine zusätzliche Service-Klasse zu implementieren. Ein Beispiel hierfür findet sich hier:
[https://howtodoinjava.com/spring-boot2/spring-boot-crud-hibernate/amp/](https://howtodoinjava.com/spring-boot2/spring-boot-crud-hibernate/amp/)

### Mehr Einfluss auf die HTTP-Response nehmen

Jede HTTP-Response wird von SpringBoot mit Header, Body und Statuscode versehen, wenn die Methode mit `@ResponseBody` annotiert ist (Teil der Composed-Annotation `@RestController`). Der Statuscode, der zurückgegeben wird, lässt sich dann auch noch mit `@ResponseStatus` anpassen.

Sofern jedoch maximal Einfluss auf die Antwort genommen werden soll, bietet es sich an, dass Methoden eine `ResponseEntity` zurückgeben wie oben geschehen.



## Links und weitere Informationen

Weitere Informationen zu Requests finden sich hier:

- [https://www.baeldung.com/spring-requestmapping](https://www.baeldung.com/spring-requestmapping)

- Webrequests mit der Powershell testen (inkl. Authentifizierung): [https://davidhamann.de/2019/04/12/powershell-invoke-webrequest-by-example/](https://davidhamann.de/2019/04/12/powershell-invoke-webrequest-by-example/)

- HTTP-Requests mit cURL testen [https://www.baeldung.com/curl-rest](https://www.baeldung.com/curl-rest)

## _Quellen und offene Ressourcen (OER)_
Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/java-springboot/addressbookapp]() und sind zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
